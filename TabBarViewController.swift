//
//  TabBarViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/22/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//  Refractored = 1/30/17
//

import UIKit

class TabBarViewController: UITabBarController {

    
    let skyBlueColor = UIColor(colorLiteralRed: 230/255, green: 246/255, blue: 252/255, alpha: 1.0)
    let pinkColor = UIColor(colorLiteralRed: 252/255, green: 96/255, blue: 97/255, alpha: 1.0)
    var longitude = 0.0
    var latitude = 0.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBar.appearance().barTintColor = skyBlueColor
        UITabBar.appearance().tintColor = pinkColor
        
        // Depending on who took an image from our thumbnail picture controller, we want to set the selected index
        // That way if someone takes a profile picture, we go back to that tab aftward
        // Until we can figure out a better way, we are setting the value in the app delegate of the view that is taking the image
        if AppDelegate.getAppDelegate().whichViewTookImage == "badge" {
            self.selectedIndex = 2
            
            // We are going to want to reset the app delegate value after each time
            AppDelegate.getAppDelegate().whichViewTookImage = ""
        }else{
            // Every other time we load the tab bar and it isnt from the badge, we want locations as the default
            self.selectedIndex = 1
            
            // We are going to want to reset the app delegate value after each time
            AppDelegate.getAppDelegate().whichViewTookImage = ""
        }

    }
}
