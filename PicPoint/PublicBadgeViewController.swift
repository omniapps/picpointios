//
//  PublicBadgeViewController.swift
//  
//
//  Created by Connor Besancenez on 1/24/17.
//  Refractored - 1/27/17
//

import UIKit
import AWSS3
import AWSCore
import NVActivityIndicatorView
import Alamofire
import SwiftyJSON

class PublicBadgeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bio: UITextView!
    @IBOutlet weak var votes: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var badgeThumbnailPicture: UIImageView!
    @IBOutlet weak var exit: UIButton!
    
    
    var userIdPassedFromPosts = String()
    var usernamePassedFromPosts = String()
    var usersPosts = [Post]()
    var usersImages = [UIImage]()
    var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    let S3BucketName: String = "picpoint"
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    let keychain = KeychainWrapper()

    
    // MARK: - View Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeUI()
        
        username.text = usernamePassedFromPosts
        
        activityIndicatorView = ActivityView.instantiateView(view: self.view)
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        // We want to hide everything and load the information only once when the view is loaded
        getUsersInfo()
    }
    
    
    // MARK: - Collection View
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return usersPosts.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! BadgeCollectionViewCell
        cell.locName.text = usersPosts[indexPath.row].locName
        cell.numberOfVotes.text = "\(usersPosts[indexPath.row].votes) votes"
        cell.imageView.image = usersImages[indexPath.row]
        return cell
    }
    
    
    // MARK: - Alamofire Requests
    func getUsersInfo() {
        self.hideUIWhileLoading()
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.unhideUIAfterLoaded()
            return
        }
        
        let headers = [
            "Auth":keychain.myObject(forKey: kSecValueData) as! String
        ]
        
        // GET /user/badge
        Alamofire.request("http://192.168.0.111:8080/user/badge?userId=\(self.userIdPassedFromPosts)", method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print(response.response?.statusCode as Any)
            
            if response.response?.statusCode == 200 {
                //We dont want to keep adding to the arrays so we reset them everytime there is a request
                self.usersPosts.removeAll()
                self.usersImages.removeAll()
                
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    // All the posts are the second array in the array
                    let count = json[1].count
                    var numberOfImages = count
                    var tempPostImages = [UIImage?](repeating: nil, count: count)
                    
                    // User information
                    self.bio.text = json[0]["description"].stringValue
                    self.votes.text = json[0]["numberOfVotes"].stringValue + " votes"
                    
                    // After we get the basic info, we want to get their profile picture
                    // We dont want to get the image though if they havent changed the default extension ("")
                    // This means they have not uploaded an image
                    let imageExt = json[0]["profileImageExt"].stringValue
                    if imageExt != "" && count > 0 {
                        // Since there is an image to get
                        // We want to make sure that we switch off the activityIndicator after we get the posts
                        self.getProfilePicture(imageExt: imageExt, postsIncluded: true)
                    }else if imageExt != "" && count == 0 {
                        // Since there is an image to get
                        // We want to make sure that we switch off the activityIndicator after the pro pic is fetched
                        self.getProfilePicture(imageExt: imageExt, postsIncluded: false)
                    }else if imageExt == "" && count == 0 {
                        // Since we dont have a pro pic to get and count is 0
                        // We just want to switch off the activity Indicator here
                        self.collectionView.reloadData()
                        self.unhideUIAfterLoaded()
                        return
                    }
                    
                    //Look to improve this in the future
                    for (index, object):(String, JSON) in (json[1]){
                        
                        //These are posts
                        let userPost = Post(_id: object["_id"].stringValue, votes: object["numberOfVotes"].int!, userId: object["userId"].stringValue, username: object["username"].stringValue, locName: object["locName"].stringValue, locId: object["locId"].stringValue, imageExt: object["imageExt"].stringValue, description: object["description"].stringValue)
                        
                        //We stored the image key as an extension in the Post
                        let S3DownloadKeyName: String = userPost.imageExt
                        let expression = AWSS3TransferUtilityDownloadExpression()
                        
                        //Here is where we will want to do stuff with the image data when it comes back asynchronously
                        //We are going to need to do something with the error
                        self.completionHandler = { (task, location, data, error) -> Void in
                            DispatchQueue.main.async(execute: {
                                if ((error) != nil){
                                    //If there is an error then we want to break out of this function and display an alert
                                    self.collectionView.reloadData()
                                    self.unhideUIAfterLoaded()
                                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We are unable to get the users posts at this time", view: self)
                                }
                                else{
                                    //No error so now we have the data
                                    let downloadedImage = UIImage(data: data!)
                                    tempPostImages[Int(index)!] = downloadedImage!
                                    
                                    //We want to decrement the number of images left so we know when we get the last image
                                    numberOfImages -= 1
                                    if numberOfImages == 0{
                                        print("Done getting images")
                                        self.usersImages = tempPostImages as! [UIImage]
                                        self.collectionView.reloadData()
                                        self.unhideUIAfterLoaded()
                                    }
                                }
                            })
                        }
                        
                        let transferUtility = AWSS3TransferUtility.default()
                        transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
                            if let _ = task.result {
                                print("Download Starting!")
                            }else{
                                self.collectionView.reloadData()
                                self.unhideUIAfterLoaded()
                                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We can not get the users posts at this time.", view: self)
                            }
                            
                            return nil
                        })
                        
                        //We only add the posts at first
                        //Once we get back all the images we reload the collection and sync the data together
                        self.usersPosts.append(userPost)
                        
                    }
                }else {
                    self.collectionView.reloadData()
                    self.unhideUIAfterLoaded()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We can not get the users posts at this time.", view: self)
                }
            }else {
                self.collectionView.reloadData()
                self.unhideUIAfterLoaded()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We can not get the users posts at this time.", view: self)
            }
        }
    }
    
    
    func getProfilePicture(imageExt: String, postsIncluded: Bool) {
        // We stored the image key as an extension in the Post
        let S3DownloadKeyName: String = imageExt
        let expression = AWSS3TransferUtilityDownloadExpression()
        
        // Here is where we will want to do stuff with the profile picture when it comes back async
        self.completionHandler = { (task, location, data, error) -> Void in
            DispatchQueue.main.async(execute: {
                if ((error) != nil){
                    // We only want to unhide if our posts are not included
                    // Meaning we have no posts to fetch and this image is the last thing needed for the UI
                    if postsIncluded == false {
                        self.unhideUIAfterLoaded()
                    }
                    
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We can not get the profile picture at this time.", view: self)
                }
                else{
                    // No error so now we have the data and want to set the profile picture
                    print("Done getting profile image")
                    self.badgeThumbnailPicture.image = UIImage(data: data!)
                    
                    // We only want to unhide if our posts are not included
                    // Meaning we have no posts to fetch and this image is the last thing needed for the UI
                    if postsIncluded == false {
                        self.unhideUIAfterLoaded()
                    }
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
            if let _ = task.result {
                print("Download Starting!")
            }else{
                // We only want to unhide if our posts are not included
                // Meaning we have no posts to fetch and this image is the last thing needed for the UI
                if postsIncluded == false {
                    self.unhideUIAfterLoaded()
                }
                
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We can not get your profile picture at this time.", view: self)
            }
            
            return nil;
        })
    }
    
    
    // MARK: - UI
    func hideUIWhileLoading() {
        self.collectionView.isHidden = true
        self.bio.isHidden = true
        self.votes.isHidden = true
        self.username.isHidden = true
        self.badgeThumbnailPicture.isHidden = true
        self.exit.isHidden = true
        
        activityIndicatorView.startAnimating()
    }
    
    
    func unhideUIAfterLoaded() {
        self.collectionView.isHidden = false
        self.bio.isHidden = false
        self.votes.isHidden = false
        self.username.isHidden = false
        self.badgeThumbnailPicture.isHidden = false
        self.exit.isHidden = false
        
        activityIndicatorView.stopAnimating()
    }
    
    
    func customizeUI() {
        _ = UIColor(colorLiteralRed: 169/255, green: 169/255, blue: 169/255, alpha: 1.0)
        
        // Setting bio
        bio.layer.borderWidth = 0
        bio.layer.borderColor = UIColor.clear.cgColor
        
        badgeThumbnailPicture.layer.cornerRadius = badgeThumbnailPicture.frame.width / 2
        badgeThumbnailPicture.layer.masksToBounds = true
    }
    
    
    // MARK: - Misc style and exit
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    @IBAction func exit(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
