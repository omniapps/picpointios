//
//  OfficialLocation.swift
//  PicPoint
//
//  Created by Devin Miller on 1/16/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import Foundation

class LocalLocation {
    
    var _id = ""
    var userId = ""
    var name = ""
    var postCount = 0
    var longitude = 0.0
    var latitude = 0.0
    var imageExt = ""
    
    init(_id: String, userId: String, name: String, postCount: Int, longitude: Double, latitude: Double, imageExt: String){
        self._id = _id
        self.userId = userId
        self.name = name
        self.postCount = postCount
        self.longitude = longitude
        self.latitude = latitude
        self.imageExt = imageExt
    }
}
