//
//  CameraViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/10/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//  Refractored - 1/30/17
//

import UIKit
import AVFoundation
import Alamofire
import CoreLocation
import SwiftyJSON
import NVActivityIndicatorView

class CameraViewController: UIViewController, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate {

    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var tempImageView: UIImageView!
    @IBOutlet weak var takePicture: UIButton!
    @IBOutlet weak var retake: UIButton!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var rotate: UIButton!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var descriptionText: UITextField!
    
    
    let keychain = KeychainWrapper()
    var locationId = String()
    var locationName = String()
    var frontOrBack = "back"
    var captureDevice:AVCaptureDevice?
    var captureSession : AVCaptureSession?
    var stillImageOutput : AVCaptureStillImageOutput?
    var previewLayer : AVCaptureVideoPreviewLayer?
    var didTakePhoto = Bool()
    var imageDataToSend: Data?
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    
    // MARK: - View Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeUI()
        setupCamera()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        cameraAccess()
        
        // Sets the preview layer to the area of the cameraView
        // Double check to see why this is here
        previewLayer?.frame = cameraView.bounds
    }
    
    
    // MARK: - Alamofire Requests
    // EVENTUALLY WE WILL WANT TO CHECK AGAIN TO SEE IF THEY ARE IN THE LOCATION RADIUS TO POST
    // THIS IS JUST A BONUS
    // Retrieves pre-signed URL from API, then posts image to Amazon S3 @ that url
    @IBAction func postPicture(_ sender: Any) {
        self.showActivityView()
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.hideActivityView()
            return
        }
        
        let parameters = ["locId" : locationId, "locName" : locationName, "description": self.descriptionText.text as Any] as [String : Any]
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        // Retrieves pre-signed URL from API
        // POST /post
        Alamofire.request("http://192.168.0.111:8080/post", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                // Grab the presigned url
                let URL = JSON(response.result.value as Any)["imageURL"].stringValue
                
                // Uploads image to Amazon S3
                Alamofire.upload(self.imageDataToSend!, to: URL, method: .put, headers: nil).response { response in
                    if response.response?.statusCode == 200 {
                        self.hideActivityView()
                        self.performSegue(withIdentifier: "backToPosts", sender: self)
                    }else{
                        self.hideActivityView()
                        AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to post at this time.", view: self)
                    }
                }
            }else {
                self.hideActivityView()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to post at this time.", view: self)
            }
        }
    }
    
    
    // MARK: - Camera Functions
    // Sets up a camera based on whether front or back is selected
    // TODO - We need to remake this to get rid of depreciations
    func setupCamera(){
        save.isHidden = true
        retake.isHidden = true
        descriptionText.text = ""
        descriptionText.isHidden = true
        
        captureSession = AVCaptureSession()
        var input = AVCaptureDeviceInput()
        
        if frontOrBack == "back"{
            captureSession?.sessionPreset = AVCaptureSessionPreset1920x1080
            captureDevice = ((AVCaptureDevice.devices()
                .filter{ ($0 as AnyObject).hasMediaType(AVMediaTypeVideo) && ($0 as AnyObject).position == .back})
                .first as? AVCaptureDevice)!
            
        }else if frontOrBack == "front"{
            captureSession?.sessionPreset = AVCaptureSessionPreset1280x720
            captureDevice = ((AVCaptureDevice.devices()
                .filter{ ($0 as AnyObject).hasMediaType(AVMediaTypeVideo) && ($0 as AnyObject).position == .front})
                .first as? AVCaptureDevice)!
        }
        
        do{
            // After we get the correct information for front or back we then want to set up and add the preview layer to the camera view
            try input = AVCaptureDeviceInput(device: captureDevice)
            captureSession?.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            
            if (captureSession?.canAddOutput(stillImageOutput))! {
                captureSession?.addOutput(stillImageOutput)
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer?.videoGravity = AVLayerVideoGravityResizeAspect
                previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.portrait
                previewLayer?.frame = cameraView.bounds
                cameraView.layer.addSublayer(previewLayer!)
                captureSession?.startRunning()
            }
        }catch{
            // Handle errors here
        }
    }
    
    
    // Takes a still image of a video, then resizes image while user is deciding whether to post or retake it
    @IBAction func takePictureButton(_ sender: Any) {
        if let videoConnection = stillImageOutput?.connection(withMediaType: AVMediaTypeVideo){
            videoConnection.videoOrientation = AVCaptureVideoOrientation.portrait
            stillImageOutput?.captureStillImageAsynchronously(from: videoConnection, completionHandler: {
                (sampleBuffer, error) in
                
                if sampleBuffer != nil {
                    self.takePicture.isHidden = true
                    self.rotate.isHidden = true
                    self.back.isHidden = true
                    self.save.isHidden = false
                    self.retake.isHidden = false
                    self.descriptionText.isHidden = false
                    
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProvider(data: imageData as! CFData)
                    let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
                    
                    // Handles scaling image while user decides whether or not to post or retake
                    // Scale this up for smaller images.. idk why
                    let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.right)
                    let size = image.size.applying(CGAffineTransform(scaleX: 0.3, y: 0.3))
                    UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
                    image.draw(in: CGRect(origin: .zero, size: size))
                    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
                    UIGraphicsEndImageContext()
                    
                    self.imageDataToSend = UIImageJPEGRepresentation(scaledImage!, 0.25)
                    self.didTakePhoto = true
                    self.tempImageView.image = image
                    self.tempImageView.isHidden = false
                    self.captureSession?.stopRunning()
                }
            })
        }
    }
    
    
    // Allows user to cancel current image, and restart attempting to take another
    @IBAction func retakePicture(_ sender: Any) {
        retake.isEnabled = false
        
        if didTakePhoto == true{
            self.save.isHidden = true
            self.retake.isHidden = true
            self.descriptionText.text = ""
            self.descriptionText.isHidden = true
            self.takePicture.isHidden = false
            self.rotate.isHidden = false
            self.back.isHidden = false
            self.captureSession?.startRunning()
            self.tempImageView.isHidden = true
            self.tempImageView.image = nil
            didTakePhoto = false
            retake.isEnabled = true
        }
    }
    
    
    @IBAction func rotateCamera(_ sender: Any) {
        if frontOrBack == "back"{
            frontOrBack = "front"
            captureSession?.stopRunning()
            setupCamera()
            
        }else if frontOrBack == "front"{
            frontOrBack = "back"
            captureSession?.stopRunning()
            setupCamera()
        }
    }
    
    
    func handlePinch(toZoom pinchRecognizer: UIPinchGestureRecognizer) {
        let pinchVelocityDividerFactor: CGFloat = 5.0
        
        // Only change something if the pinch/zoom was on the capture device
        if pinchRecognizer.state == .changed {
            if let device = captureDevice {
                do {
                    try device.lockForConfiguration()
                    let desiredZoomFactor = device.videoZoomFactor + CGFloat(atan2f(Float(pinchRecognizer.velocity), Float(pinchVelocityDividerFactor)))
                    
                    // Check if desiredZoomFactor fits required range from 1.0 to activeFormat.videoMaxZoomFactor
                    device.videoZoomFactor = max(1.0, min(desiredZoomFactor, device.activeFormat.videoMaxZoomFactor))
                    device.unlockForConfiguration()
                }catch {
                    // Handle erros
                }
            }
        }
    }
    
    
    // Handles what happens when the user is asked whether we can have access to their camera or not
    // In viewWillAppear so they are asked everything they go to the camera if they don't say yes
    func cameraAccess() {
        let cameraMediaType = AVMediaTypeVideo
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "cameraAccessVC") as! CameraAccessViewController
        var cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
        
        // Checks what camera authorization status the user has
        switch cameraAuthorizationStatus {
        case .denied, .restricted:
            // Go to cameraAccessVC because they denied allowing acces or are restricted
            self.present(vc, animated: true, completion: nil)
            
        case .authorized:
            // Set up camera because they accepted camera access
            setupCamera()
            
        case .notDetermined:
            // Prompts user for the permission to use the camera
            // If user says no, dismiss VC
            AVCaptureDevice.requestAccess(forMediaType: cameraMediaType, completionHandler: { granted in
                
                // Need to put on main thread due to UI or something
                DispatchQueue.main.async {
                    if granted {
                        cameraAuthorizationStatus = .authorized
                        self.setupCamera()
                    } else {
                        cameraAuthorizationStatus = .notDetermined
                        // Go to cameraAccessVC because they denied allowing access
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    
    // MARK: - Custom UI and Misc
    // Focus the screen on touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // We want do 2 different things depending on if they have taken a picture or not
        if didTakePhoto == true{
            self.descriptionText.resignFirstResponder()
        }else{
            let touchPoint = touches.first! as UITouch
            
            // Setting up the variables just for camera focus
            let screenSize = cameraView.bounds.size
            let focusPoint = CGPoint(x: touchPoint.location(in: cameraView).y / screenSize.height, y: 1.0 - touchPoint.location(in: cameraView).x / screenSize.width)
            
            // Look into docs for more info on focusing and lock/unlock
            if let device = captureDevice {
                do {
                    try device.lockForConfiguration()
                    if device.isFocusPointOfInterestSupported {
                        device.focusPointOfInterest = focusPoint
                        device.focusMode = AVCaptureFocusMode.autoFocus
                    }
                    if device.isExposurePointOfInterestSupported {
                        device.exposurePointOfInterest = focusPoint
                        device.exposureMode = AVCaptureExposureMode.autoExpose
                    }
                    device.unlockForConfiguration()
                } catch {
                    // Handles errors
                }
            }
        }
    }
    
    
    // Handle functionality after taking picture
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "backToPosts" {
            // Checks if there is no internet, if there is none, it breaks out of function
            if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            }else {
                // Setting PostViewController values when segue
                // We need to pass these value back so we can segue to the posts instead of going back to the location VC to get the post feed info
                let nextView = segue.destination as? PostsViewController
                nextView?.locationName = locationName
                nextView?.locationId = locationId
            }
        }
    }
    
    
    func customizeUI() {
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(self.handlePinch))
        pinch.delegate = self
        self.view.addGestureRecognizer(pinch)
        
        // Setting shadows so we can see white buttons even on a white background
        back.layer.shadowColor = UIColor.black.cgColor
        back.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        back.layer.shadowOpacity = 0.4
        
        rotate.layer.shadowColor = UIColor.black.cgColor
        rotate.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        rotate.layer.shadowOpacity = 0.4
        
        takePicture.layer.shadowColor = UIColor.black.cgColor
        takePicture.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        takePicture.layer.shadowOpacity = 0.4
        
        save.layer.shadowColor = UIColor.black.cgColor
        save.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        save.layer.shadowOpacity = 0.4
        
        retake.layer.shadowColor = UIColor.black.cgColor
        retake.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        retake.layer.shadowOpacity = 0.4
        
        // Added slight shadow to status bar so you can see white text if the background is white
        let statusBarView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 20))
        statusBarView.backgroundColor = UIColor.black
        statusBarView.layer.opacity = 0.05
        
        self.view.addSubview(statusBarView)
    }
    
    
    // Changes status bar text to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func showActivityView() {
        self.save.isEnabled = false
        self.activityIndicatorView.startAnimating()
    }
    
    
    func hideActivityView() {
        self.save.isEnabled = true
        self.activityIndicatorView.stopAnimating()
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
