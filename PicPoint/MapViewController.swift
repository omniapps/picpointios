//
//  MapViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/30/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//  Refractored - 1/30/17
//

import UIKit
import MapKit
import CoreLocation
import SwiftyJSON
import Alamofire

class MapViewController: UIViewController, MKMapViewDelegate{
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    
    let pinkColor = UIColor(colorLiteralRed: 252/255, green: 96/255, blue: 97/255, alpha: 1.0)
    let keychain = KeychainWrapper()
    var hasLoadedLocation = false
    var mapLocations = [LocalLocation]()
    var latitude = String()
    var longitude = String()
    var locationName = String()
    var userLocationShown = Bool()
    var myDropPin = MKPointAnnotation()
    
    
    // MARK: - View Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
    }
    

    override func viewWillAppear(_ animated: Bool) {
        // Get the location from the tab bar
        // It is updated everytime we get our location when we query local locations
        let tbvc = self.tabBarController  as! TabBarViewController
        
        // Check to see if the local location changed 
        if String(tbvc.longitude) != longitude && String(tbvc.latitude) != latitude {
            longitude = String(tbvc.longitude)
            latitude = String(tbvc.latitude)
            
            // Reset the mapView every time the view appears
            // This is because we pass the location every time
            // Having overlaying pins and overlays would be bad
            mapView.removeOverlays(mapView.overlays)
            mapView.removeAnnotations(mapView.annotations)
            
            loadMap()
        }
    }
    
    
    // MARK: - Alamofire Requests
    // Fetches locations within 2.5 miles in each direction
    func getMapLocations() {
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops..", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        Alamofire.request("http://192.168.0.111:8080/locations/mapview/map?latitude=\(latitude)&longitude=\(longitude)", method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
    
            if response.response?.statusCode == 200 {
                // We want to reset the loctions that we currently have
                self.mapLocations.removeAll()
                
                if let value = response.result.value {
                    let json = JSON(value)
                
                    for(_, location): (String, JSON) in json{
                        let mapLocation = LocalLocation(_id: location["_id"].stringValue, userId: location["userID"].stringValue, name: location["name"].stringValue, postCount: location["numberOfPosts"].int!,longitude: location["loc"][0].double!, latitude: location["loc"][1].double!, imageExt: location["imageExt"].stringValue)
                        self.mapLocations.append(mapLocation)
                    }
                    self.dropPins()
                }else {
                    AppDelegate.getAppDelegate().alert(title: "Oops..", message: "We were unable to load the map locations at this time.", view: self)
                }
            }else {
                AppDelegate.getAppDelegate().alert(title: "Oops..", message: "We were unable to load the map locations at this time.", view: self)
            }
        }
    }
    
    
    // MARK: - Map Functions
    func loadMap() {
        let myLocation = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
        let span = MKCoordinateSpanMake(0.06, 0.06)
        let region = MKCoordinateRegionMake(myLocation, span)
        mapView.showsUserLocation = false
        mapView.setRegion(region, animated: true)
        mapView.userTrackingMode = MKUserTrackingMode.none
        addUserCircle(location: CLLocation(latitude: Double(latitude)!, longitude: Double(longitude)!))
        getMapLocations()
        mapView.tintColor = pinkColor
    }
    
    
    // Adds pins to map
    func dropPins() {
        // Drop out location seperatly 
        myDropPin = MKPointAnnotation()
        myDropPin.coordinate = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
        myDropPin.title = "My Location"
        mapView.addAnnotation(myDropPin)
        
        // Drop all of the normal locations
        for location in mapLocations {
            let dropPin = MKPointAnnotation()
            dropPin.coordinate = CLLocationCoordinate2DMake(location.latitude, location.longitude)
            dropPin.title = location.name
            dropPin.subtitle = "\(location.postCount) posts"
            mapView.addAnnotation(dropPin)
        }
    }
    
    
    // Presents posts based on which pin was tapped
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        // Setting posts view controller so we can pass info once it presents
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "postsVC") as! PostsViewController
        
        // Need to find which location they clicked on so we can pass the location id, location name, and post count
        for location in mapLocations {
            if location.name == (view.annotation?.title)! {
                vc.locationName = ((view.annotation?.title)!)!
                vc.locationId = location._id
                vc.postCount = location.postCount
                
                // Disable the camera if they are not allowed to post because the pin is outside the radius (400 feet)
                if CLLocation.distance(from: myDropPin.coordinate, to: (view.annotation?.coordinate)!) > 121.92 {
                    vc.hideCamera = true
                }
            }
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    // Handles what the view looks like and shows for each pin once tapped
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Checks to make sure the pin is not the user location pin so it doesn't add a pin annotation
        if annotation is MKUserLocation {
            return nil
        }
        
        // Custom button that is added to pin annotation view
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.setImage(UIImage(named:"pinkRightArrow.png"), for: .normal)
        
        // Adds pin annotation with custom right button that will segue to the specific location
        let annotationView: MKPinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        annotationView.canShowCallout = true
        
        // We only want blue if it is out location
        // If it isn't then we add the callout image to the regular location pin
        if let annotationTitle = annotation.title {
            if annotationTitle == "My Location" {
                annotationView.pinTintColor = UIColor.blue
            }else {
                annotationView.rightCalloutAccessoryView = button
            }
        }
        
        return annotationView
    }
    
    
    // Adds a ring around the user with a radius of 400 feet to symbolize what locations are in their posting range
    func addUserCircle(location: CLLocation) {
        let circle = MKCircle(center: location.coordinate, radius: 121.92 as CLLocationDistance)
        self.mapView.add(circle)
    }
    
    
    // Checks if there is a circle overlay and if so, edits it and places it
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = pinkColor
            circle.lineWidth = 1
            return circle
        }else {
            return MKPolylineRenderer()
        }
    }
    
    
    // This allows us to go back to the users location
    @IBAction func centerUserButton(_ sender: Any) {
        self.mapView.setCenter(myDropPin.coordinate, animated: true)
    }
}


extension CLLocation {
    // In meters
    class func distance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance {
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return from.distance(from: to)
    }
}
