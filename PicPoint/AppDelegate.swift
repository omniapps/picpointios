//
//  AppDelegate.swift
//  PicPoint
//
//  Created by Devin Miller on 1/9/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UIGestureRecognizerDelegate {

    var window: UIWindow?
    var reach = Reach()
    var whichViewTookImage = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // AWS set up
        let credentialsProvider = AWSCognitoCredentialsProvider(
            regionType: AWSRegionType.usWest2, identityPoolId: "us-west-2:91cc2a60-dad5-4f2c-a3f2-3fa420d81d30")
        let defaultServiceConfiguration = AWSServiceConfiguration(
            region: AWSRegionType.usWest2, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = defaultServiceConfiguration
        
        let pinkColor = UIColor(colorLiteralRed: 252/255, green: 96/255, blue: 97/255, alpha: 1.0)
        UITextView.appearance().tintColor = pinkColor
        UITextField.appearance().tintColor = pinkColor
        
        // Log previous user out and delete the keychain
        if UserDefaults.standard.object(forKey: "FirstRun") == nil {
            
        }

        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    class func getAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    func alert(title: String, message: String, view: UIViewController) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        view.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Keychain
    func deleteAllKeysForSecClass(secClass: CFTypeRef) {
        let dict: [NSString : AnyObject] = [kSecClass : secClass]
        let result = SecItemDelete(dict as CFDictionary)
        assert(result == noErr || result == errSecItemNotFound, "Error deleting keychain data (\(result))")
    }
    
    
    func resetKeychain() {
        self.deleteAllKeysForSecClass(secClass: kSecClassGenericPassword)
        self.deleteAllKeysForSecClass(secClass: kSecClassInternetPassword)
        self.deleteAllKeysForSecClass(secClass: kSecClassCertificate)
        self.deleteAllKeysForSecClass(secClass: kSecClassKey)
        self.deleteAllKeysForSecClass(secClass: kSecClassIdentity)
    }
}

