//
//  PendingLocationTableViewCell.swift
//  PicPoint
//
//  Created by Devin Miller on 1/15/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit

class PendingLocationTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var flagBTN: UIButton!
    @IBOutlet weak var pendingLocationName: UILabel!
    @IBOutlet weak var pendingLocationImage: UIImageView!
    
    
    var flag: ((UITableViewCell) -> Void)?

    
    @IBAction func flag(_ sender: Any) {
        flag?(self)
        
        //Possibly turn the flag red and stuff
    }
}
