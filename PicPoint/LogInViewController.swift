//
//  LogInViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/9/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit

class LogInViewController: UIViewController, UITextFieldDelegate{

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet var login: UIButton!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet weak var needAccount: UIButton!
    
    let userDefaults = UserDefaults.standard
    let keychain = KeychainWrapper()

    
    // MARK: - View Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeUI()
        
        self.username.delegate = self
        self.password.delegate = self
    }

    
    override func viewDidAppear(_ animated: Bool) {
        // The user is on a fresh install but there is a token in the keychain
        if userDefaults.bool(forKey: "hasRunBefore") == false && keychain.myObject(forKey: kSecValueData) as? String != "password"{
            resetKeychainAndDeleteToken()
            // The app is not a fresh install and there is a token in the keychain
        }else if userDefaults.bool(forKey: "hasRunBefore") == true && keychain.myObject(forKey: kSecValueData) as? String != "password"{
            self.performSegue(withIdentifier: "sessionActive", sender: self)
        }
    }
    
    
    // MARK: - Alamofire Requests
    @IBAction func logInButton(_ sender: Any) {
        self.view.isUserInteractionEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.view.isUserInteractionEnabled = true
            return
        }
        
        // We are going to want to check here because if they have not run before, 
        // Then we want to wipe the keychain and delete any token they might have had
        // This will really only get called if there was an error in the ViewDidAppear resetKeychainAndDeleteToken()
        if userDefaults.bool(forKey: "hasRunBefore") == false && self.keychain.myObject(forKey: kSecValueData) as? String != "password"{
            deleteTokenBeforeLogIn()
        }else {
            // There was no token to delete so we just go straight to log in
            logUserIn()
        }
        
    }
    
    
    // There was no token to delete so we just go straight to log in
    func logUserIn() {
        //Assigning JSON parameters
        let parameters = [ "username": self.username.text,
                           "password": self.password.text ]
        
        // POST /users/login
        Alamofire.request("http://192.168.0.111:8080/user/login", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            if response.response?.allHeaderFields["Auth"] != nil{
                let json = JSON(response.result.value as Any)
                
                //Set the token in the Keychain
                self.keychain.mySetObject(response.response?.allHeaderFields["Auth"], forKey: kSecValueData)
                
                //Set the username in the Keychain
                self.keychain.mySetObject(self.username.text, forKey: kSecAttrAccount)
                
                //Set the id in the keychain
                self.keychain.mySetObject(json["_id"].stringValue, forKey: kSecAttrLabel)
                self.keychain.writeToKeychain()
                
                self.performSegue(withIdentifier: "logIn", sender: self)
            }else {
                var message = String()
                
                // Checks why user can't log in and alerts if credentials are wrong or whether there was a problem
                if let value = response.result.value {
                    let json = JSON(value)
                    message = json["message"].stringValue
                }
                
                if message == "Credentials Wrong" {
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "Your login credentials were incorrect. Please try again.", view: self)
                } else {
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "Sorry, we were not able to log you in at this time.", view: self)
                }
                
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    
    // If there is a token, we want to delete it and make sure the request comes back before we log in
    func deleteTokenBeforeLogIn() {
        let headers = [
            "Auth": self.keychain.myObject(forKey: kSecValueData) as! String
        ]
        
        // DELETE /users/logout
        Alamofire.request("http://192.168.0.111:8080/user/logout", method: .delete, parameters: nil, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 || response.response?.statusCode == 204 {
                // Reset the keychain so the tokens are gone after they are deleted on the database
                self.resetKeychain()
                
                // Set the NSUserDefault so we know it has ran for the first time
                self.userDefaults.setValue(true, forKey: "hasRunBefore")
                self.userDefaults.synchronize()
                
                // Finally we log in if there was no error
                self.logUserIn()
            }else {
                // We could not delete the token upon logging in for the first time
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "Sorry, we were not able to log you in at this time.", view: self)
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    
    // This is called when the app first gets installed
    func resetKeychainAndDeleteToken() {
        self.view.isUserInteractionEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.view.isUserInteractionEnabled = true
            return
        }
        
        let headers = [
            "Auth": self.keychain.myObject(forKey: kSecValueData) as! String
        ]
        
        // DELETE /users/logout
        Alamofire.request("http://192.168.0.111:8080/user/logout", method: .delete, parameters: nil, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 || response.response?.statusCode == 204 {
                // Reset the keychain so the tokens are gone after they are deleted on the database
                self.resetKeychain()
                
                // Set the NSUserDefault so we know it has ran for the first time
                self.userDefaults.setValue(true, forKey: "hasRunBefore")
                self.userDefaults.synchronize()
                
                self.view.isUserInteractionEnabled = true
            }else {
                // There was an error so now we just want to call this again when they try to log in
                self.view.isUserInteractionEnabled = true
            }
        }
    }
    
    
    // MARK: - UI and Misc
    // Handles all customized UI elements
    func customizeUI() {
        let pinkColor = UIColor(colorLiteralRed: 252/255, green: 96/255, blue: 97/255, alpha: 1.0)
        let darkGrayColor = UIColor(colorLiteralRed: 211/255, green: 211/255, blue: 211/255, alpha: 1.0)
        
        // Setting mapView region
        let world = MKCoordinateRegionForMapRect(MKMapRectWorld)
        mapView.region = world
        
        // Changes buttons
        login.backgroundColor = pinkColor
        login.layer.cornerRadius = 5
        needAccount.tintColor = darkGrayColor
        
        // Change textFields
        username.layer.cornerRadius = 5
        username.layer.borderWidth = 1.5
        username.layer.borderColor = darkGrayColor.cgColor
        username.backgroundColor = UIColor.white
        username.frame = CGRect(x: 16, y: 271, width: self.view.frame.width - 32, height: 40)
        username.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 0))
        username.leftViewMode = .always
        
        password.layer.cornerRadius = 5
        password.layer.borderWidth = 1.5
        password.layer.borderColor = darkGrayColor.cgColor
        password.backgroundColor = UIColor.white
        password.frame = CGRect(x: 16, y: 319, width: self.view.frame.width - 32, height: 40)
        password.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 0))
        password.leftViewMode = .always
    }
    
    
    // Dismisses keyboard when touches off keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    // Dismisses keyboard when clicks return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.username.resignFirstResponder()
        self.password.resignFirstResponder()
        return true
    }
    
    
    // MARK: - Keychain
    func deleteAllKeysForSecClass(secClass: CFTypeRef) {
        let dict: [NSString : AnyObject] = [kSecClass : secClass]
        let result = SecItemDelete(dict as CFDictionary)
        assert(result == noErr || result == errSecItemNotFound, "Error deleting keychain data (\(result))")
    }
    
    
    func resetKeychain() {
        self.deleteAllKeysForSecClass(secClass: kSecClassGenericPassword)
        self.deleteAllKeysForSecClass(secClass: kSecClassInternetPassword)
        self.deleteAllKeysForSecClass(secClass: kSecClassCertificate)
        self.deleteAllKeysForSecClass(secClass: kSecClassKey)
        self.deleteAllKeysForSecClass(secClass: kSecClassIdentity)
    }
}
