//
//  ThumbnailPictureViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/24/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//  Refractored - 1/30/17
//

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class ThumbnailPictureViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var tempImageView: UIImageView!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var takePicture: UIButton!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var rotate: UIButton!
    @IBOutlet weak var retake: UIButton!
    @IBOutlet weak var pendingLocationName: UILabel!
    
    
    let keychain = KeychainWrapper()
    var locationName = String()
    var latitude = String()
    var longitude = String()
    var whichViewTookImage = String()
    var frontOrBack = "back"
    var captureDevice:AVCaptureDevice?
    var captureSession : AVCaptureSession?
    var stillImageOutput : AVCaptureStillImageOutput?
    var previewLayer : AVCaptureVideoPreviewLayer?
    var didTakePhoto = Bool()
    var imageDataToSend: Data?
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    
    // MARK: - View Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCamera()
        customizeUI()
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        cameraAccess()
        
        // Sets the preview layer to the area of the cameraView
        // Double check to see why this is here
        previewLayer?.frame = cameraView.bounds
    }
    
    
    // MARK: - Alamofire Requests
    @IBAction func saveButton(_ sender: Any) {
        // We don't want the ability to spam save
        self.showActivityView()
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.hideActivityView()
            return
        }
        
        // We have this view controller being used by adding pending locations and adding a profile picture
        // That is why we need to check this
        if whichViewTookImage == "pendingLocation" {
            // These parameters are passed from the first view before the camera
            let parameters = [ "name"         : locationName,
                               "longitude"    : longitude,
                               "latitude"     : latitude ] as [String : Any]
            
            let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
            
            // POST /pending/location
            // This passes back a pre-signed url
            Alamofire.request("http://192.168.0.111:8080/pending/location", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.response?.statusCode == 200 {
                    // Get the presigned url to upload the image
                    let URL = JSON(response.result.value as Any)["imageURL"].stringValue
                    
                    // Uploads image to Amazon S3 
                    Alamofire.upload(self.imageDataToSend!, to: URL, method: .put, headers: nil).response { response in
                        if response.response?.statusCode == 200 {
                            self.hideActivityView()
                            
                            // Until we can figure out a better way, we are setting the value in the app delegate of the view that is taking the image
                            // This value will be used by the tab bar so we know which tab to go back to after taking an image
                            AppDelegate.getAppDelegate().whichViewTookImage = self.whichViewTookImage
                            
                            self.performSegue(withIdentifier: "doneTakingPicture", sender: self)
                        }else {
                            self.hideActivityView()
                            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to save this location.", view: self)
                        }
                    }
                }else {
                    self.hideActivityView()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to save this location.", view: self)
                }
                
            }
        }else {
            // We are adding a circle image as a profile picture
            let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
            
            // POST user/badge/image
            // This route replaces a pro pic if there is one and if not just adds one
            Alamofire.request("http://192.168.0.111:8080/user/badge/image", method: .post, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if response.response?.statusCode == 200 {
                    let URL = JSON(response.result.value as Any)["imageURL"].stringValue
                    
                    // Uploads image to Amazon S3 using pre-signed url
                    Alamofire.upload(self.imageDataToSend!, to: URL, method: .put, headers: nil).response { response in
                        if response.response?.statusCode == 200 {
                            self.hideActivityView()
                            
                            // Until we can figure out a better way, we are setting the value in the app delegate of the view that is taking the image
                            // This value will be used by the tab bar so we know which tab to go back to after taking an image
                            AppDelegate.getAppDelegate().whichViewTookImage = self.whichViewTookImage
                            
                            self.performSegue(withIdentifier: "doneTakingPicture", sender: self)
                        }else {
                            self.hideActivityView()
                            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to save your profile picture.", view: self)
                        }
                    }
                }else {
                    self.hideActivityView()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to save your profile picture.", view: self)
                }
            }
        }
    }
    
    
    // MARK: - Camera Functions
    // Sets up a camera based on whether front or back is selected
    // TODO - We need to remake this to get rid of depreciations
    func setupCamera() {
        save.isHidden = true
        retake.isHidden = true
        
        captureSession = AVCaptureSession()
        var input = AVCaptureDeviceInput()
        
        if frontOrBack == "back"{
            captureSession?.sessionPreset = AVCaptureSessionPreset1920x1080
            captureDevice = ((AVCaptureDevice.devices()
                .filter{ ($0 as AnyObject).hasMediaType(AVMediaTypeVideo) && ($0 as AnyObject).position == .back})
                .first as? AVCaptureDevice)!
            
        }else if frontOrBack == "front"{
            captureSession?.sessionPreset = AVCaptureSessionPreset1280x720
            captureDevice = ((AVCaptureDevice.devices()
                .filter{ ($0 as AnyObject).hasMediaType(AVMediaTypeVideo) && ($0 as AnyObject).position == .front})
                .first as? AVCaptureDevice)!
        }
        
        do{
            // After we get the correct information for front or back we then want to set up and add the preview layer to the camera view
            try input = AVCaptureDeviceInput(device: captureDevice)
            captureSession?.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            
            if (captureSession?.canAddOutput(stillImageOutput))! {
                captureSession?.addOutput(stillImageOutput)
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
                previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.portrait
                
                // Change the camera view and temp image view to circles
                makeCircularView(cameraView, toDiameter: Int(self.view.frame.width))
                makeCircularView(tempImageView, toDiameter: Int(self.view.frame.width))
                
                previewLayer?.frame = cameraView.bounds
                cameraView.layer.addSublayer(previewLayer!)
                captureSession?.startRunning()
            }
        }catch{
            // Handle errors here
        }
    }
    
    
    @IBAction func takePictureButton(_ sender: Any) {
        takePicture.isEnabled = false
        if let videoConnection = stillImageOutput?.connection(withMediaType: AVMediaTypeVideo){
            videoConnection.videoOrientation = AVCaptureVideoOrientation.portrait
            
            stillImageOutput?.captureStillImageAsynchronously(from: videoConnection, completionHandler: {
                (sampleBuffer, error) in
                
                if sampleBuffer != nil {
                    self.takePicture.isHidden = true
                    self.rotate.isHidden = true
                    self.back.isHidden = true
                    self.save.isHidden = false
                    self.retake.isHidden = false
                    
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProvider(data: imageData as! CFData)
                    let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
                    
                    // Handles scaling image while user decides whether or not to post or retake
                    // Scale this up for smaller images.. idk why
                    let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.right)
                    
                    // Crop the image to a circle using the extension
                    let newImage = image.circle
                    
                    self.imageDataToSend = UIImageJPEGRepresentation(newImage!, 0.7)
                    self.didTakePhoto = true
                    self.tempImageView.image = image
                    self.tempImageView.isHidden = false
                    self.captureSession?.stopRunning()
                }
            })
        }
        takePicture.isEnabled = true
    }
    
    
    // Allows user to cancel current image, and restart attempting to take another
    // This could use some refractoring
    @IBAction func retakePicture(_ sender: Any) {
        retake.isEnabled = false
        
        if didTakePhoto == true{
            self.save.isHidden = true
            self.retake.isHidden = true
            self.takePicture.isHidden = false
            self.rotate.isHidden = false
            self.back.isHidden = false
            self.captureSession?.startRunning()
            self.tempImageView.isHidden = true
            self.tempImageView.image = nil
            didTakePhoto = false
            retake.isEnabled = true
        }
    }
  
    
    @IBAction func rotateCamera(_ sender: Any) {
        if frontOrBack == "back"{
            frontOrBack = "front"
            captureSession?.stopRunning()
            setupCamera()
            
        }else if frontOrBack == "front"{
            frontOrBack = "back"
            captureSession?.stopRunning()
            setupCamera()
        }
    }

    
    // This is so we can make the camera view and the temp image view circles
    // This is called in the setupCamera func
    func makeCircularView(_ circularView: UIView, toDiameter: Int) {
        cameraView.clipsToBounds = true
        tempImageView.clipsToBounds = true
        let center = view.center
        let newFrame = CGRect(x: CGFloat(circularView.frame.origin.x), y: CGFloat(circularView.frame.origin.y), width: CGFloat(toDiameter), height: CGFloat(toDiameter))
        circularView.frame = newFrame
        circularView.layer.cornerRadius = CGFloat(toDiameter / 2)
        circularView.center = center
    }
    
    
    // Handles what happens when the user is asked whether we can have access to their camera or not
    // In viewWillAppear so they are asked everything they go to the camera if they don't say yes
    func cameraAccess() {
        let cameraMediaType = AVMediaTypeVideo
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "cameraAccessVC") as! CameraAccessViewController
        var cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
        
        // Checks what camera authorization status the user has
        switch cameraAuthorizationStatus {
        case .denied, .restricted:
            // Go to cameraAccessVC because they denied allowing acces or are restricted
            self.present(vc, animated: true, completion: nil)
            
        case .authorized:
            // Set up camera because they accepted camera access
            setupCamera()
            
        case .notDetermined:
            // Prompts user for the permission to use the camera
            // If user says no, dismiss VC
            AVCaptureDevice.requestAccess(forMediaType: cameraMediaType, completionHandler: { granted in
                
                // Need to put on main thread due to UI or something
                DispatchQueue.main.async {
                    if granted {
                        cameraAuthorizationStatus = .authorized
                        self.setupCamera()
                    } else {
                        cameraAuthorizationStatus = .notDetermined
                        // Go to cameraAccessVC because they denied allowing access
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    
    // MARK: - Custom UI and Misc
    // Focus the screen on touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchPoint = touches.first! as UITouch
        
        // Setting up the variables just for camera focus
        let screenSize = cameraView.bounds.size
        let focusPoint = CGPoint(x: touchPoint.location(in: cameraView).y / screenSize.height, y: 1.0 - touchPoint.location(in: cameraView).x / screenSize.width)
        
        // Look into docs for more info on focusing and lock/unlock
        if let device = captureDevice {
            do {
                try device.lockForConfiguration()
                if device.isFocusPointOfInterestSupported {
                    device.focusPointOfInterest = focusPoint
                    device.focusMode = AVCaptureFocusMode.autoFocus
                }
                if device.isExposurePointOfInterestSupported {
                    device.exposurePointOfInterest = focusPoint
                    device.exposureMode = AVCaptureExposureMode.autoExpose
                }
                device.unlockForConfiguration()
            } catch {
                // Handles errors
            }
        }
    }
    
    
    func customizeUI() {
        // We only want a label there if there are adding a location image
        if whichViewTookImage == "pendingLocation" {
            pendingLocationName.text = locationName
        }
    }
    
    
    // Changes status bar text to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func showActivityView() {
        self.save.isEnabled = false
        self.activityIndicatorView.startAnimating()
    }
    
    
    func hideActivityView() {
        self.save.isEnabled = true
        self.activityIndicatorView.stopAnimating()
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        // We want to make sure we go back to the view we came from
        AppDelegate.getAppDelegate().whichViewTookImage = self.whichViewTookImage
        
        self.dismiss(animated: true, completion: nil)
    }
}


// Crops image taken into circle
extension UIImage {
    var circle: UIImage? {
        let square = CGSize(width: min(size.width / 4, size.height / 4), height: min(size.width / 4, size.height / 4))
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: square))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
}
