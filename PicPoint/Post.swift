//
//  Post.swift
//  PicPoint
//
//  Created by Devin Miller on 1/16/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import Foundation

class Post {
    
    var _id = ""
    var votes = 0
    var userId = ""
    var username = ""
    var locName = ""
    var locId = ""
    var imageExt = ""
    var postImage = UIImage()
    var description = ""
    var currentUserHasVoted = false
    var voteValue = 0
    
    init(_id: String, votes: Int, userId: String, username: String, locName: String, locId: String, imageExt: String, description: String){
        self._id = _id
        self.votes = votes
        self.userId = userId
        self.username = username
        self.locName = locName
        self.locId = locId
        self.imageExt = imageExt
        self.description = description
    }
}
