//
//  PendingLocationDescriptionTableViewCell.swift
//  
//
//  Created by Connor Besancenez on 1/30/17.
//
//

import UIKit

class PendingLocationDescriptionTableViewCell: UITableViewCell {
    @IBOutlet weak var howToText: UILabel!
}
