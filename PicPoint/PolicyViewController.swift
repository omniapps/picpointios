//
//  PolicyViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 2/15/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit

class PolicyViewController: UIViewController {

    @IBOutlet weak var whichPolicy: UILabel!
    @IBOutlet weak var privacyPolicyText: UITextView!
    @IBOutlet weak var eulaPolicyText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
