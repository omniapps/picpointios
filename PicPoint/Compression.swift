//
//  Compression.swift
//  PicPoint
//
//  Created by Connor Besancenez on 1/20/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import Foundation

public class Compress {
    class func compressImage(image:UIImage) -> NSData{
        var actualHeight: CGFloat = image.size.height
        var actualWidth: CGFloat = image.size.width
        let maxHeight: CGFloat = 600.0
        let maxWidth: CGFloat = 800.0
        var imgRatio: CGFloat = actualWidth / actualHeight
        let maxRatio: CGFloat = maxWidth / maxHeight
        let compressionQuality: CGFloat = 0.5
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData: NSData = UIImageJPEGRepresentation(img, compressionQuality)! as NSData
        UIGraphicsEndImageContext()
        
        //print(imageData.length)//Check file size
        
        //return UIImage(data: imageData as Data)!
        
        return imageData
    }

}
