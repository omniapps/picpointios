//
//  BadgeCollectionViewCell.swift
//  
//
//  Created by Connor Besancenez on 1/22/17.
//
//

import UIKit

class BadgeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var locName: UILabel!
    @IBOutlet weak var numberOfVotes: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}
