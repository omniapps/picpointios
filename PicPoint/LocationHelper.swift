//
//  LocationHelper.swift
//  
//
//  Created by Connor Besancenez on 1/24/17.
//
//

import Foundation
import UIKit
import CoreLocation

class LocationHelper {
    
    static func initLocation(view: UIViewController){
        let locationManager = CLLocationManager()
        
        // Checks authorization status of location services
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse:
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .denied, .restricted:
            let vc = view.storyboard?.instantiateViewController(withIdentifier: "locationServicesVC") as! LocationServicesViewController
            view.present(vc, animated: true, completion: nil)
        }
    }
}
