//
//  PlacesViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/11/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class PlacesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Making sure location services are enabled
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Checks authorization status of location services
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways:
            break
        case .authorizedWhenInUse:
            break
        case .denied, .restricted, .notDetermined:
            self.performSegue(withIdentifier: "ls3", sender: self)
        }
    }
    
    // Delegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isOfficial == false{
            return self.pendingLocations.count
        }else{
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "place") as! PlaceTableViewCell
        
        if isOfficial == true {
            cell.textLabel?.text = "official"
        } else {
            print(pendingLocations.count)
            cell.textLabel?.text = self.pendingLocations[indexPath.row].name
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isOfficial == true {
            return "Point of Interests in your area."
        } else {
            return "Pending Point of Interests in your area."
        }
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        
        if isOfficial == true {
            
            // Checks if there is no internet, if there is none, it breaks out of function
            if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
                AppDelegate.getAppDelegate().alert(title: "Opps.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
                return false
            } else {
                return true
            }
        } else {
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextView = segue.destination as? PostsViewController
        
        // Pass the selected object to the new view controller.
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        if identifier == "toPosts" {

            // Checks if there is no internet, if there is none, it breaks out of function
            if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
                AppDelegate.getAppDelegate().alert(title: "Opps.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
                return false
            }
        }
        return true
    }
    
    // Buttons that toggle between data on the tableView
    @IBAction func officialButton(_ sender: Any) {
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Opps.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        // Change to unoffial tableView data
        if isOfficial == false {
            isOfficial = true
            official.isEnabled = false
            unofficial.isEnabled = true
            self.tableView.reloadData()
        }
    }
    
    @IBAction func profileButton(_ sender: Any) {
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Opps.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        self.performSegue(withIdentifier: "profile", sender: self)
    }
    
    @IBAction func unofficialButton(_ sender: Any) {
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Opps.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        if isOfficial == true {
            // Change to unoffial tableView data
            isOfficial = false
            official.isEnabled = true
            unofficial.isEnabled = false
            self.tableView.reloadData()
        }
    }
    
    func getPendingLocations() {
        
        
        //Create an instance of the keychain
        let keychain = KeychainWrapper()
        let headers = [
            "Auth": keychain.myObject(forKey: kSecValueData) as! String
        ]
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Opps.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        // Set location
        let longitude = String(currentLocation.longitude)
        let latitude  = String(currentLocation.latitude)
        
        // POST /pending/location
        Alamofire.request("http://192.168.0.111:3000/pending/locations/loc?longitude=\(longitude)&latitude=\(latitude)", method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            self.pendingLocations.removeAll()
            print(longitude)
            print(latitude)
            print(response.result.value)
            print(response.response?.statusCode)
            
            if response.response?.statusCode == 200 {
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    for(_,location):(String, JSON) in json {
                        var pendingLocation = PendingLocation(_id: location["_id"].string!, userId: location["userId"].string!, name: location["name"].string!)
                        self.pendingLocations.append(pendingLocation)
                    }
                    
                    //We have the
                    self.tableView.reloadData()
                }else{
                    //Alert
                }
            }else {
                AppDelegate.getAppDelegate().alert(title: "Opps.", message: "We were unable to load the data. Check your internet and location services please.", view: self)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("err")
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("im here")
        currentLocation = locations[0].coordinate as CLLocationCoordinate2D

        if currentLocation != nil{
            locationManager.stopUpdatingLocation()
            print("Got location")
            getPendingLocations()
        }
    }
    
    // Segues to AddLocationViewController
    @IBAction func addLocationButton(_ sender: Any) {
        self.performSegue(withIdentifier: "addLocation", sender: self)
    }
    
    // Hide status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
