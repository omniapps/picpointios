//
//  ActivityViewHelper.swift
//  PicPoint
//
//  Created by Connor Besancenez on 1/24/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class ActivityView {
    static func instantiateView(view: UIView) -> NVActivityIndicatorView{
        let pinkColor = UIColor(colorLiteralRed: 252/255, green: 96/255, blue: 97/255, alpha: 1.0)
        
        // Set up custom activity view
        let activitySize = CGRect(x: view.frame.size.width/2 - 100, y: view.frame.size.height/2 - 100, width: 200, height: 200)
        let activityColor = pinkColor
        let activityIndicatorView = NVActivityIndicatorView(frame: activitySize, type: .ballScale, color: activityColor, padding: 0.0)
        view.addSubview(activityIndicatorView)
        
        return activityIndicatorView
    }
}
