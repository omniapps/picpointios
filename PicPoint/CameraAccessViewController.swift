//
//  CameraAccessViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 2/13/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit

class CameraAccessViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // Sends user to iPhone settings so they can allow camera access
    @IBAction func allowCameraAccessButton(_ sender: Any) {
        let settingsUrl = NSURL(string:UIApplicationOpenSettingsURLString) as! URL
        UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
    }
    
    
    // Segues back to locationsViewController if they don't want to allow camera access
    @IBAction func dontAllowCameraAccessButton(_ sender: Any) {
        performSegue(withIdentifier: "dontAllowCameraAccess", sender: self)
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
