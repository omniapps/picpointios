//
//  Vote.swift
//  PicPoint
//
//  Created by Devin Miller on 1/18/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import Foundation

class Vote {
    
    var voteValue = 0
    var userId = ""
    var postId = ""
    var locId = ""
    var currentUserHasVoted = false
    
    init(voteValue: Int, userId: String, postId: String, locId: String, currentUserHasVoted: Bool){
        self.voteValue = voteValue
        self.userId = userId
        self.postId = postId
        self.locId = locId
        self.currentUserHasVoted = currentUserHasVoted
    }
}
