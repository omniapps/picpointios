//
//  SignUpViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/9/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//


import UIKit
import Alamofire
import MapKit
import SwiftyJSON

class SignUpViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var signUp: UIButton!
    @IBOutlet weak var alreadyHaveAccount: UIButton!
    @IBOutlet var name: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var reach = Reach()
    let skyBlueColor = UIColor(colorLiteralRed: 230/255, green: 246/255, blue: 252/255, alpha: 1.0)
    let pinkColor = UIColor(colorLiteralRed: 252/255, green: 96/255, blue: 97/255, alpha: 1.0)
    let darkGrayColor = UIColor(colorLiteralRed: 211/255, green: 211/255, blue: 211/255, alpha: 1.0)
    let greenColor = UIColor(colorLiteralRed: 114/255, green: 255/255, blue: 168/255, alpha: 1.0)
    let redColor = UIColor(colorLiteralRed: 255/255, green: 150/255, blue: 150/255, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signUp.isEnabled = false
        self.email.delegate = self
        self.username.delegate = self
        self.password.delegate = self
        self.confirmPassword.delegate = self
        
        customizeUI()
    }
    
    
    @IBAction func privacyPolicyButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "policyVC") as! PolicyViewController
        self.present(vc, animated: true, completion: nil)
        
        vc.whichPolicy.text = "Privacy Policy"
        vc.eulaPolicyText.isHidden = true
        vc.privacyPolicyText.isHidden = false
    }
    
    
    @IBAction func eulaPolicyButton(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "policyVC") as! PolicyViewController
        self.present(vc, animated: true, completion: nil)
        
        vc.whichPolicy.text = "End User License Agreement"
        vc.eulaPolicyText.isHidden = false
        vc.privacyPolicyText.isHidden = true
    }
    
    
    @IBAction func signUpButton(_ sender: Any) {
        
        signUp.isEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        // Sign up user with API
        // Assign JSON parameters
        let parameters = [ "email"   : email.text,
                           "username": username.text,
                           "password": password.text ]
        
        // POST /users
        Alamofire.request("http://192.168.0.111:8080/user", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            print(response.response?.statusCode as Any)
            print(response.result.value as Any)
            
            if response.response?.statusCode == 200 {
                self.performSegue(withIdentifier: "signedUp", sender: self)
            } else {
                
                // Checks why user can't sign up and alerts if username is taken, email is taken, or there was another error signing up
                if let value = response.result.value {
                    let json = JSON(value)
                    let message = json["message"].stringValue
                    
                    if message == "Username taken" {
                        AppDelegate.getAppDelegate().alert(title: "Oops.", message: "Bummer, someone already has this username. Get creative and try again!", view: self)
                    } else if message == "Email taken"{
                        AppDelegate.getAppDelegate().alert(title: "Oops.", message: "Your email has already been used with another account. If you don't remember your account, contact support to help.", view: self)
                    } else {
                        AppDelegate.getAppDelegate().alert(title: "Oops.", message: "Sorry, we were not able to sign you up at this time.", view: self)
                    }
                }
            }
        }
    }
    
    
    // Checking all text fields for errors
    // if no errors, they can sign up
    // if there are errors, button is disabled
    @IBAction func emailEditingChanged(_ sender: UITextField) {
        
        checkTextFieldsForErrors()
        
        if (email.text?.contains("@"))! && (email.text?.contains("."))! {
            email.layer.borderColor = greenColor.cgColor
        } else {
            email.layer.borderColor = redColor.cgColor
        }
    }
    
    
    @IBAction func usernameEditingChanged(_ sender: UITextField) {
        
        checkTextFieldsForErrors()
        
        if (username.text?.characters.count)! > 0 && (username.text?.characters.count)! < 16 {
            username.layer.borderColor = greenColor.cgColor
        } else {
            username.layer.borderColor = redColor.cgColor
        }
    }
    
    
    @IBAction func passwordEditingChanged(_ sender: UITextField) {
        
        checkTextFieldsForErrors()
        
        if confirmPassword.text! == password.text! && (password.text?.characters.count)! < 26 && (password.text?.characters.count)! > 5{
            password.layer.borderColor = greenColor.cgColor
            confirmPassword.layer.borderColor = greenColor.cgColor
        } else {
            password.layer.borderColor = redColor.cgColor
        }
    }
    
    
    @IBAction func confirmPasswordEditingChanged(_ sender: UITextField) {
        
        checkTextFieldsForErrors()
        
        if confirmPassword.text! == password.text! && (confirmPassword.text?.characters.count)! < 26 && (confirmPassword.text?.characters.count)! > 5{
            confirmPassword.layer.borderColor = greenColor.cgColor
            password.layer.borderColor = greenColor.cgColor
        } else {
            confirmPassword.layer.borderColor = redColor.cgColor
        }
    }
    
    
    func checkTextFieldsForErrors() {
        if  email.text! == "" || username.text! == "" || password.text! == "" || confirmPassword.text! == "" || !(email.text?.contains("@"))! || !(email.text?.contains("."))! || (password.text?.characters.count)! > 25 || (password.text?.characters.count)! < 6 || password.text != confirmPassword.text || (username.text?.characters.count)! > 15{
            
            signUp.isEnabled = false
        } else {
            signUp.isEnabled = true
        }
    }
    
    
    // Handles all customized UI elements
    func customizeUI() {
        
        // Styles textFields
        email.layer.cornerRadius = 5
        email.layer.borderWidth = 1.5
        email.layer.borderColor = darkGrayColor.cgColor
        email.backgroundColor = UIColor.white
        email.frame = CGRect(x: 16, y: 118, width: self.view.frame.width - 32, height: 40)
        email.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 0))
        email.leftViewMode = .always
        
        username.layer.cornerRadius = 5
        username.layer.borderWidth = 1.5
        username.layer.borderColor = darkGrayColor.cgColor
        username.backgroundColor = UIColor.white
        username.frame = CGRect(x: 16, y: 116, width: self.view.frame.width - 32, height: 40)
        username.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 0))
        username.leftViewMode = .always
        
        password.layer.cornerRadius = 5
        password.layer.borderWidth = 1.5
        password.layer.borderColor = darkGrayColor.cgColor
        password.backgroundColor = UIColor.white
        password.frame = CGRect(x: 16, y: 214, width: self.view.frame.width - 32, height: 40)
        password.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 0))
        password.leftViewMode = .always
        
        confirmPassword.layer.cornerRadius = 5
        confirmPassword.layer.borderWidth = 1.5
        confirmPassword.layer.borderColor = darkGrayColor.cgColor
        confirmPassword.backgroundColor = UIColor.white
        confirmPassword.frame = CGRect(x: 16, y: 262, width: self.view.frame.width - 32, height: 40)
        confirmPassword.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 0))
        confirmPassword.leftViewMode = .always
        
        // Styles buttons
        signUp.backgroundColor = pinkColor
        signUp.layer.cornerRadius = 5
        alreadyHaveAccount.tintColor = darkGrayColor
        
        // Styles label
        name.textColor = darkGrayColor
        
        // Setting mapView region
        let world = MKCoordinateRegionForMapRect(MKMapRectWorld)
        mapView.region = world
    }
    
    
    // Dismisses keyboard when touches off keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func backToLoginButton(_ sender: Any) {
        self.performSegue(withIdentifier: "backToLogin", sender: self)
    }
    
    
    // Dismisses keyboard when clicks return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.username.resignFirstResponder()
        self.password.resignFirstResponder()
        self.confirmPassword.resignFirstResponder()
        self.email.resignFirstResponder()
        return true
    }
}
