//
//  PostTableViewCell.swift
//  PicPoint
//
//  Created by Devin Miller on 1/11/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class PostTableViewCell: UITableViewCell {

    
    @IBOutlet weak var flagBTN: UIButton!
    @IBOutlet var votes: UILabel!
    @IBOutlet var postImage: UIImageView!
    @IBOutlet var upVote: UIButton!
    @IBOutlet var downVote: UIButton!
    @IBOutlet weak var usernameBTN: UIButton!
    @IBOutlet weak var descriptionText: UITextField!
    

    var currentUserHasVoted = false
    var up:       ((UITableViewCell) -> Void)?
    var down:     ((UITableViewCell) -> Void)?
    var flag:     ((UITableViewCell) -> Void)?
    var username: ((UITableViewCell) -> Void)?

    
    // For whatever fucking reason we have to reset the cells data after every "reuse"
    override func prepareForReuse() {
        upVote.setImage(UIImage(named: "upVote.png"), for: .normal)
        downVote.setImage(UIImage(named: "downVote.png"), for: .normal)
        upVote.isEnabled = true
        downVote.isEnabled = true
    }
    
    
    // Setting shadows so we can see white buttons even on a white background
    override func awakeFromNib() {
        flagBTN.layer.shadowColor = UIColor.black.cgColor
        flagBTN.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        flagBTN.layer.shadowOpacity = 0.4
        
        votes.layer.shadowColor = UIColor.black.cgColor
        votes.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        votes.layer.shadowOpacity = 0.4
        
        upVote.layer.shadowColor = UIColor.black.cgColor
        upVote.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        upVote.layer.shadowOpacity = 0.4
        
        downVote.layer.shadowColor = UIColor.black.cgColor
        downVote.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        downVote.layer.shadowOpacity = 0.4
        
        usernameBTN.layer.shadowColor = UIColor.black.cgColor
        usernameBTN.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        usernameBTN.layer.shadowOpacity = 0.4
    }
    
    
    @IBAction func upVote(_ sender: Any) {
        up?(self)
        
        // Disable upvote button
        upVote.isEnabled = false
        
        // Enable downvote button
        downVote.isEnabled = true
        
        // Adds 1 or 2 depending on if they are voting for the first time, or changing their vote
        if currentUserHasVoted == false {
            currentUserHasVoted = true
            votes.text = String(Int(votes.text!)! + 1)
        }else {
            votes.text = String(Int(votes.text!)! + 2)
        }
        
        // Symbolize they upvoted
        upVote.setImage(UIImage(named: "upVoteHighlighted.png"), for: .normal)
        
        // Unhighlight downvote
        downVote.setImage(UIImage(named: "downVote.png"), for: .normal)
    }
    
    @IBAction func downVote(_ sender: Any) {
        down?(self)
        
        // Disable downvote button
        downVote.isEnabled = false
        
        // Enable upvote button
        upVote.isEnabled = true
        
        // Adds 1 or 2 depending on if they are voting for the first time, or changing their vote
        if currentUserHasVoted == false {
            currentUserHasVoted = true
            votes.text = String(Int(votes.text!)! - 1)
        }else {
            votes.text = String(Int(votes.text!)! - 2)
        }
        
        // Symbolize they downvoted
        downVote.setImage(UIImage(named: "downVoteHighlighted.png"), for: .normal)
        
        // Unhighlight upvote
        upVote.setImage(UIImage(named: "upVote.png"), for: .normal)
    }
    
    @IBAction func flag(_ sender: Any) {
        flag?(self)
        
        //Turn the flag red and shit
    }
    
    
    @IBAction func goToBadge(_ sender: Any) {
        username?(self)
    }
}
