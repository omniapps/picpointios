//
//  FooterTableViewCell.swift
//  Pods
//
//  Created by Devin Miller on 1/15/17.
//
//

import UIKit
import MapKit
import Alamofire

class FooterTableViewCell: UITableViewCell {

    
    @IBOutlet var save: UIButton!
    
    @IBOutlet var pointOfInterestName: UITextField!
    @IBOutlet var mapView: MKMapView!
    
    @IBAction func saveButton(_ sender: Any) {
        
        save.isEnabled = false
        
        // Alert that informs user that the point of interest name is empty
        if pointOfInterestName.text == "" {
            AppDelegate.getAppDelegate().alert(title: "Opps.", message: "Please enter a Point of Interest name to save it as a location for you and others to post at!", view: self)
            return
        }
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Opps.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        // Set location
        longitude = String(currentLocation.longitude)
        latitude = String(currentLocation.latitude)
        
        // Sign up user with API
        // Assign JSON parameters
        let parameters = [
            "name"         : pointOfInterestName.text,
            "longitude"    : longitude,
            "latitude"     : latitude
            ] as [String : Any]
        
        //Create an instance of the keychain
        let keychain = KeychainWrapper()
        
        var headers = [
            "Auth": keychain.myObject(forKey: kSecValueData) as! String
        ]
        
        // POST /pending/location
        Alamofire.request("http://192.168.0.111:3000/pending/location", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            
            print(response.response?.statusCode)
            
            if response.response?.statusCode == 200 {
                self.performSegue(withIdentifier: "back", sender: self)
            }else {
                AppDelegate.getAppDelegate().alert(title: "Opps.", message: "We were unable to save this location.", view: self)
            }
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
