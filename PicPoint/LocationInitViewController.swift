//
//  LocationInitViewController.swift
//  PicPoint
//
//  Created by Connor Besancenez on 2/15/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class LocationInitViewController: UIViewController, CLLocationManagerDelegate {

    
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
    }
    
    
    // For now we want this until we are sure that the didchangeauth function gets called every time
    override func viewDidAppear(_ animated: Bool) {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }else if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            self.performSegue(withIdentifier: "locationAllowed", sender: self)
        }else if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "locationServicesVC") as! LocationServicesViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    func customizeUI() {
        // Setting mapView region
        let world = MKCoordinateRegionForMapRect(MKMapRectWorld)
        mapView.region = world
    }
    
    
    // MARK: - Location Services
    // This function gets called when we assign the CLLocation delegate
    // This acts a a view did load for getting posts ... sort of
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            self.performSegue(withIdentifier: "locationAllowed", sender: self)
        }else if status == .denied || status == .restricted {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "locationServicesVC") as! LocationServicesViewController
            self.present(vc, animated: true, completion: nil)
        }else if status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }
}
