//
//  LocationsTableViewCell.swift
//  
//
//  Created by Devin Miller on 1/15/17.
//
//

import UIKit

class LocationsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var mapBTN: UIButton!
    @IBOutlet weak var flagBTN: UIButton!
    @IBOutlet weak var cameraBTN: UIButton!
    @IBOutlet var postNumber: UILabel!
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    
    
    var flag: ((UITableViewCell) -> Void)?
    var camera: ((UITableViewCell) -> Void)?
    var map: ((UITableViewCell) -> Void)?
  
    
    @IBAction func flag(_ sender: Any) {
        flag?(self)
    }
    
    
    @IBAction func map(_ sender: Any) {
        map?(self)
    }
    
    
    @IBAction func camera(_ sender: Any) {
        camera?(self)
    }
}
