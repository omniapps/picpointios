//
//  PopupMapViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/22/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//  Refractored - 1/30/17
//

import UIKit
import MapKit
import CoreLocation
import SwiftyJSON
import Alamofire

class PopupMapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var back: UIButton!
    
    
    var latitude = String()
    var longitude = String()
    var locationName = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self

        loadSingleLocation()
    }
    

    // Loads single location that was selected to be shown on the map from locationsVC
    func loadSingleLocation() {
        
        let location = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
        let span = MKCoordinateSpanMake(0.01, 0.01)
        let region = MKCoordinateRegionMake(location, span)
        mapView.setRegion(region, animated: true)
        
        let dropPin = MKPointAnnotation()
        dropPin.coordinate = CLLocationCoordinate2DMake(Double(latitude)!, Double(longitude)!)
        dropPin.title = locationName
        mapView.addAnnotation(dropPin)
    }

    
    // Dismisses popUpMapViewController and shows locationsVC
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
