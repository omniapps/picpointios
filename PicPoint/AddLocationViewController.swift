//
//  AddLocationViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/13/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//  Refractored - 2/1/17
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import AWSS3
import AWSCore
import NVActivityIndicatorView

class AddLocationViewController: UIViewController, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    
    @IBOutlet var locationName: UITextField!
    @IBOutlet weak var camera: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var exit: UIButton!
    @IBOutlet weak var seperator1: UIView!
    @IBOutlet weak var seperator2: UIView!
    
    
    let grayColor = UIColor(colorLiteralRed: 211/255, green: 211/255, blue: 211/255, alpha: 1.0)
    let keychain = KeychainWrapper()
    let locationManager = CLLocationManager()
    var currentLocation = CLLocationCoordinate2D()
    var pendingLocations = [PendingLocation]()
    var pendingLocationsImages = [UIImage]()
    var longitude = String()
    var latitude  = String()
    var gotLocation = false
    var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    let S3BucketName: String = "picpoint"
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var refreshControl = UIRefreshControl()
    
    
    // MARK: - View Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicatorView = ActivityView.instantiateView(view: self.view)
        
        self.locationName.delegate = self
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        // Add the functionality for dismissing keyboard
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(gesture:)))
        self.view.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false
        
        self.tableView.addSubview(self.refreshControl)
        
        // Set up location manager
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Only load if the auth status is good on initial load
        if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            self.disableUIWhileLoading()
            locationManager.startUpdatingLocation()
        }
        
        customizeUI()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        // We only want to do this if the user didnt get location initially in view did load
        // This would happen if the user loaded the app with their location services on deny
        if (CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse) && gotLocation == false {
            self.disableUIWhileLoading()
            locationManager.startUpdatingLocation()
        }
    }
    
    
    // MARK: - Table View Functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pendingLocations.count + 1
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 101
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        // Centers the text in the footer and header I believe
        if view is UITableViewHeaderFooterView {
            let tableViewHeaderFooterView: UITableViewHeaderFooterView? = (view as? UITableViewHeaderFooterView)
            tableViewHeaderFooterView?.textLabel?.textAlignment = .center
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pendingLocation") as! PendingLocationTableViewCell
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "text") as! PendingLocationDescriptionTableViewCell
        let str = "If you don't see your desired location to post at, add a name and take a picture of the location. To become an official location, one other person has to has to verify its legitimacy."
        
        // This is the first cell so we want to display the instructions for making a pending location
        if indexPath.row == 0 {
            cell2.howToText?.text = str
            return cell2
        }
        
        // Normal cells
        cell.pendingLocationName.text = pendingLocations[indexPath.row - 1].name
        cell.pendingLocationImage.layer.cornerRadius = cell.pendingLocationImage.frame.width / 2
        cell.pendingLocationImage.layer.masksToBounds = true
        cell.pendingLocationImage.image = pendingLocationsImages[indexPath.row - 1]
        
        // Function in the cell controller for flagging
        cell.flag = { (cell) in
            self.flagPendingLocation(row: indexPath.row)
        }
        
        return cell
    }
    
    
    // MARK: - Alamofire Requests
    // Handles when a user selects a row to confirm it's legitimacy
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        // Instantiate the alert controller for now
        // May want to create a custom one or do something different
        let alert = UIAlertController(title: "Are you sure?", message: "Do you want to confirm the legitimacy of this location so that you and others may post to it?", preferredStyle: UIAlertControllerStyle.alert)
        
        
        // Confirms they want the location official
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            // We only want to disable the UI here if they select yes to confirm the locaiton
            self.disableUIWhileLoading()
            
            // We need to pass the id of a row minus one because our first row is a decription 
            // Otherwise out array would be out of index
            let parameters = [ "_id" : self.pendingLocations[indexPath.row - 1]._id]
            
            let headers = [ "Auth": self.keychain.myObject(forKey: kSecValueData) as! String ]
            
            // POST /pending/location/confirm
            Alamofire.request("http://192.168.0.111:8080/pending/location/confirm", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                
                if response.response?.statusCode == 200 {
                    self.tableView.deselectRow(at: indexPath, animated: true)
                    self.getPendingLocations()
                    
                // Error when same user tries verifying their pending location
                }else if response.response?.statusCode == 404 {
                    self.enableUIAfterLoaded()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "Nice try! We need two people to confirm a location's legitimacy. Get a friend to verify this location!", view: self)
                    tableView.deselectRow(at: indexPath, animated: true)
                }else {
                    self.enableUIAfterLoaded()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to verify this location at this time.", view: self)
                    tableView.deselectRow(at: indexPath, animated: true)
                }
            }
        }))
        
        // User does NOT want to confirm the location
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action: UIAlertAction!) in
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }))
        
        present(alert, animated: true, completion: nil)
    }
    

    func getPendingLocations() {
        self.view.isUserInteractionEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.enableUIAfterLoaded()
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        Alamofire.request("http://192.168.0.111:8080/pending/locations/loc?latitude=\(latitude)&longitude=\(longitude)", method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    //All the posts are the first array in the array
                    let count = json.count
                    var numberOfImages = count
                    
                    // We want to assign temp arrays to avoid index out of range
                    var tempPendingLocations = [PendingLocation]()
                    var tempPendingLocationImages = [UIImage?](repeating: nil, count: count)
                    
                    // If there are no pending locations we want to break out of this
                    if count == 0 {
                        self.enableUIAfterLoaded()
                        AppDelegate.getAppDelegate().alert(title: "There are no pending locations here yet.", message: "Add a pending location here now!", view: self)
                        return
                    }
                    
                    for(index, location): (String, JSON) in json{
                        let pendingLocation = PendingLocation(_id: location["_id"].stringValue, userId: location["userID"].stringValue, name: location["name"].stringValue, imageExt: location["imageExt"].stringValue)
                        
                        // We want to add the pending location even before the images come in
                        tempPendingLocations.append(pendingLocation)
                        
                        //We stored the image key as an extension in the Post
                        let S3DownloadKeyName: String = pendingLocation.imageExt
                        let expression = AWSS3TransferUtilityDownloadExpression()
                        
                        //Here is where we will want to do stuff wit hthe image data when it comes back asynchronously
                        //We are going to need to do something with the error
                        self.completionHandler = { (task, location, data, error) -> Void in
                            DispatchQueue.main.async(execute: {
                                if ((error) != nil){
                                    self.enableUIAfterLoaded()
                                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "There was an error getting the pending locaitons.", view: self)
                                }else{
                                    //No error so now we have the data
                                    let downloadedImage = UIImage(data: data!)
                                    tempPendingLocationImages[Int(index)!] = downloadedImage!
                                    
                                    numberOfImages -= 1
                                    if numberOfImages == 0{
                                        print("Done getting images")
                                        //Assign the temp arrays to the actual arrays
                                        self.pendingLocations = tempPendingLocations
                                        self.pendingLocationsImages = tempPendingLocationImages as! [UIImage]
                                        self.enableUIAfterLoaded()
                                    }
                                }
                            })
                        }
                        
                        let transferUtility = AWSS3TransferUtility.default()
                        transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
                            if let _ = task.result {
                                print("Download Starting!")
                            }else {
                                self.enableUIAfterLoaded()
                                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to load the pending locations.", view: self)
                            }
                            
                            return nil;
                        })
                    }
                }else {
                    self.enableUIAfterLoaded()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to load the pending locations.", view: self)
                }
            }else {
                self.enableUIAfterLoaded()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to load the pending locations.", view: self)
            }
        }
    }

    
    func flagPendingLocation(row: Int){
        self.disableUIWhileLoading()
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.enableUIAfterLoaded()
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        //We need to check which picker view we are in so we know which id to pass
        let parameters = [
            "_id": self.pendingLocations[row - 1]._id
        ]
        
        // POST /pending/location/flag
        Alamofire.request("http://192.168.0.111:8080/pending/location/flag", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                self.enableUIAfterLoaded()
                AppDelegate.getAppDelegate().alert(title: "Thank you.", message: "We will be looking into this pending location.", view: self)
            }else {
                self.enableUIAfterLoaded()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to flag the pending location at this time", view: self)
            }
        }
    }

    
    // MARK: - UI
    // Handles all customized UI elements
    func customizeUI() {
        // Style Buttons
        camera.layer.cornerRadius = 5
        
        // Styles textFields
        locationName.layer.cornerRadius = 5
        locationName.layer.borderWidth = 1.5
        locationName.backgroundColor = UIColor.white
        locationName.layer.borderColor = grayColor.cgColor
        locationName.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 0))
        locationName.leftViewMode = .always
        
        // Sets the section title sperators where they need to be on all screens
        seperator1.frame = CGRect(x: 0, y: 10, width: (self.view.frame.width - 149) / 2 , height: 1)
        seperator2.frame = CGRect(x: ((self.view.frame.width - 149) / 2) + 149 , y: 10, width: (self.view.frame.width - 149) / 2, height: 1)
    }
    
    
    func disableUIWhileLoading(){
        self.activityIndicatorView.startAnimating()
    }
    
    
    func enableUIAfterLoaded(){
        self.tableView.reloadData()
        self.activityIndicatorView.stopAnimating()
        self.view.isUserInteractionEnabled = true
        self.refreshControl.endRefreshing()
    }
    
    
    // Makes status bar black
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    // MARK: - Segues
    // Segues to circular camera where they can take a picture for a pending location and save it
    @IBAction func cameraButton(_ sender: Any) {
        camera.isEnabled = false
        
        // Alert that informs user that the point of interest name is empty
        if locationName.text == "" {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "Please enter a location name before you move on to take a picture.", view: self)
            camera.isEnabled = true
            return
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "thumbnailPicVC") as! ThumbnailPictureViewController
        vc.whichViewTookImage = "pendingLocation"
        vc.locationName = locationName.text!
        vc.latitude = latitude
        vc.longitude = longitude
        self.present(vc, animated: true, completion: nil)
        camera.isEnabled = true
    }
    
    
    // MARK: - Misc
    func dismissKeyboard(gesture: UITapGestureRecognizer) {
        self.locationName.resignFirstResponder()
    }
    
    
    // Dismisses keyboard when clicks return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    // We needed to add this so it doesnt refresh until they let go
    // Without this it would refresh once it hit a certain point
    // Then it would glitch out or jump because the alamofire request was firing right away
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // If we dont check for refreshing, it will call this everytime we stop scroll in the tableview
        if refreshControl.isRefreshing {
            refresh()
        }
    }
    
    
    func refresh() {
        if CLLocationManager.locationServicesEnabled() {
            // Here we need to reset the variables so we only get the location once
            self.gotLocation = false
            locationManager.startUpdatingLocation()
        }
    }

    
    @IBAction func exit(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Only loads locations once
        if gotLocation == false {
            gotLocation = true
            
            let loc = locations[0]
            let myLocation = CLLocationCoordinate2DMake(loc.coordinate.latitude, loc.coordinate.longitude)
            latitude  = String(myLocation.latitude)
            longitude = String(myLocation.longitude)
            
            getPendingLocations()
        }else {
            // May or may not need the first line
            //self.enableUIAfterLoaded()
            locationManager.stopUpdatingLocation()
        }
    }
}
