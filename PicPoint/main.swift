//
//  main.swift
//  PicPoint
//
//  Created by Devin Miller on 1/21/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import Foundation


import UIKit

UIApplicationMain(Process.argc, Process.unsafeArgv, NSStringFromClass(TimerUIApplication), NSStringFromClass(AppDelegate))
