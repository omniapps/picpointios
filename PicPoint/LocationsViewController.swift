//
//  LocationsViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/15/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import AWSS3
import AWSCore
import NVActivityIndicatorView

// I need to make it so I disable getting more when it is loading and then show when local or global are loading
class LocationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UIScrollViewDelegate {

    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var segControl: UISegmentedControl!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var seperator1: UIView!
    @IBOutlet weak var seperator2: UIView!
    
    
    let skyBlueColor = UIColor(colorLiteralRed: 230/255, green: 246/255, blue: 252/255, alpha: 1.0)
    let pinkColor = UIColor(colorLiteralRed: 252/255, green: 96/255, blue: 97/255, alpha: 1.0)
    let keychain = KeychainWrapper()
    let S3BucketName: String = "picpoint"
    let refreshControl = UIRefreshControl()
    let locationManager = CLLocationManager()
    var globalLocations = [GlobalLocation]()
    var localLocations = [LocalLocation]()
    var lastGlobalLocation: GlobalLocation?
    var lastLocalLocation: LocalLocation?
    var globalLocationIds = [String]()
    var localLocationsIds = [String]()
    var localLocationsImages = [UIImage]()
    var globalLocationsImages = [UIImage]()
    var hasGottenGlobalLocations = false
    var longitude = String()
    var latitude  = String()
    var locationName = String()
    var postCount = Int()
    var locationID = String()
    var hasFoundLocation = Bool()
    var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    
    // MARK: - View Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.setNeedsStatusBarAppearanceUpdate()
        
        // We only need to get the global locations on pull to refresh and when the view loads
        hasGottenGlobalLocations = false

        customizeUI()
        
        activityIndicatorView = ActivityView.instantiateView(view: self.view)
        
        // Might have to update to .refreshControl = self.refreshControl
        self.tableView.addSubview(self.refreshControl)
        
        // We only want the locaton once when we call updating location
        hasFoundLocation = false
        
        // Only set up location once upon load
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Only load if the auth status is good on initial load
        if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            self.activityIndicatorView.startAnimating()
            locationManager.startUpdatingLocation()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        // We only want to do this if the user didnt get location initially in view did load
        // This would happen if the user loaded the app with their location services on deny
        if (CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse) && hasFoundLocation == false {
            self.activityIndicatorView.startAnimating()
            locationManager.startUpdatingLocation()
        }
    }
    
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segControl.selectedSegmentIndex == 0{
            return localLocations.count
        }else{
            return globalLocations.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "location") as! LocationsTableViewCell
        print(indexPath.row)
        if segControl.selectedSegmentIndex == 0 {
            // Checks whether there is 1 post or multiple posts. Proper grammar
            if localLocations[indexPath.row].postCount == 1 {
                cell.postNumber.text = "\(localLocations[indexPath.row].postCount) post"
                cell.postNumber.textColor = pinkColor
            }else {
                cell.postNumber.text = "\(localLocations[indexPath.row].postCount) posts"
                cell.postNumber.textColor = pinkColor
            }
            
            cell.locationName.text = localLocations[indexPath.row].name
            cell.locationImage.layer.cornerRadius = cell.locationImage.frame.height/2
            cell.locationImage.layer.masksToBounds = true
            cell.locationImage.image = localLocationsImages[indexPath.row]
            cell.cameraBTN.isHidden = false
        }else {
            // Checks whether there is 1 post or multiple posts. Proper grammar
            if globalLocations[indexPath.row].postCount == 1 {
                cell.postNumber.text = "\(globalLocations[indexPath.row].postCount) post"
                cell.postNumber.textColor = pinkColor
            }else {
                cell.postNumber.text = "\(globalLocations[indexPath.row].postCount) posts"
                cell.postNumber.textColor = pinkColor
            }
            
            cell.locationName.text = globalLocations[indexPath.row].name
            cell.locationImage.layer.cornerRadius = cell.locationImage.frame.height/2
            cell.locationImage.layer.masksToBounds = true
            cell.locationImage.image = globalLocationsImages[indexPath.row]
            cell.cameraBTN.isHidden = true
        }
        
        cell.flag = { (cell) in
            self.flagLocation(row: indexPath.row)
        }
        cell.camera = { (cell) in
            self.camera(row: indexPath.row)
        }
        cell.map = { (cell) in
            self.map(row: indexPath.row)
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if view is UITableViewHeaderFooterView {
            let tableViewHeaderFooterView: UITableViewHeaderFooterView? = (view as? UITableViewHeaderFooterView)
            tableViewHeaderFooterView?.textLabel?.textAlignment = .center
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Sets values to pass to PostsViewController when segue
        if segControl.selectedSegmentIndex == 0{
            locationName = localLocations[indexPath.row].name
            postCount    = localLocations[indexPath.row].postCount
            locationID   = localLocations[indexPath.row]._id
        }else if segControl.selectedSegmentIndex == 1{
            locationName = globalLocations[indexPath.row].name
            postCount    = globalLocations[indexPath.row].postCount
            locationID   = globalLocations[indexPath.row]._id
        }
        
        // Instantiate view controller so we can present it instead of segue
        // This is because we dismiss this view controller and we just want to remain consistent
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "postsVC") as! PostsViewController
    
        // Set all of the properties in the postsviewcontroller
        vc.locationName = locationName
        vc.postCount = postCount
        vc.locationId = locationID
        
        // We dont want to allow the camera to be functionable if it is a global post feed
        if segControl.selectedSegmentIndex == 1 {
            vc.hideCamera = true
        }
        
        // Deselect the row for UI purposes
        self.tableView.deselectRow(at: indexPath, animated: false)
        self.present(vc, animated: true, completion: nil)
    }
    
    
    // MARK: - Alamofire Requests
    func getLocalLocations() {
        self.view.isUserInteractionEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.enableUIAfterLoaded()
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        Alamofire.request("http://192.168.0.111:8080/locations/loc?latitude=\(latitude)&longitude=\(longitude)", method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                if let value = response.result.value {
                    let json = JSON(value)
                
                    // All the posts are the first array in the array
                    let count = json.count
                    var numberOfImages = count
                    
                    // We need temp arrays instead of removing all of the data before I get new data
                    // This prevents index out of range
                    var tempLocations = [LocalLocation]()
                    var tempLocationIds = [String]()
                    var tempLocationImages = [UIImage?](repeating: nil, count: count)
                
                    // If there are no locations we want to exit
                    if count == 0 {
                        self.enableUIAfterLoaded()
                        AppDelegate.getAppDelegate().alert(title: "No locations here yet.", message: "Be the first to create a location here!", view: self)
                        return
                    }
                    
                    for(index, location): (String, JSON) in json{
                        let location = LocalLocation(_id: location["_id"].stringValue, userId: location["userID"].stringValue, name: location["name"].stringValue, postCount: location["numberOfPosts"].int!, longitude: location["loc"][0].double!, latitude: location["loc"][1].double!, imageExt: location["imageExt"].stringValue)
                        
                        // We want to add the location even before the images come in
                        tempLocations.append(location)
                        tempLocationIds.append(location._id)
                        self.lastLocalLocation = location

                        // We want to use a stock image if there is no extension
                        if location.imageExt == "" {
                            tempLocationImages[Int(index)!] = UIImage(named: "locationPictureComingSoon")
                            
                            numberOfImages -= 1
                            if numberOfImages == 0 {
                                print("Done getting images")
                                // We got the images so now we want to assign the data to the actual arrays
                                self.localLocations = tempLocations
                                self.localLocationsIds = tempLocationIds
                                self.localLocationsImages = tempLocationImages as! [UIImage]

                                self.enableUIAfterLoaded()
                            }
                        }else {
                            // We stored the image key as an extension in the Post
                            let S3DownloadKeyName: String = location.imageExt
                            let expression = AWSS3TransferUtilityDownloadExpression()
                            
                            // Here is where we will want to do stuff wit hthe image data when it comes back asynchronously
                            // We are going to need to do something with the error
                            self.completionHandler = { (task, location, data, error) -> Void in
                                DispatchQueue.main.async(execute: {
                                    if ((error) != nil){
                                        self.enableUIAfterLoaded()
                                        AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get locations around you at this time.", view: self)
                                    }else {
                                        // No error so now we have the data
                                        let downloadedImage = UIImage(data: data!)
                                        tempLocationImages[Int(index)!] = downloadedImage!
                                        
                                        numberOfImages -= 1
                                        if numberOfImages == 0{
                                            print("Done getting images")
                                            // We got the images so now we want to assign the data to the actual arrays
                                            self.localLocations = tempLocations
                                            self.localLocationsIds = tempLocationIds
                                            self.localLocationsImages = tempLocationImages as! [UIImage]
                                            
                                            self.enableUIAfterLoaded()
                                        }
                                    }
                                })
                            }
                            
                            let transferUtility = AWSS3TransferUtility.default()
                            transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
                                if let _ = task.result {
                                    print("Download Starting!")
                                }else {
                                    self.enableUIAfterLoaded()
                                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get locations around you at this time.", view: self)
                                }
                                
                                return nil
                            })
                        }
                    }
                }else {
                    self.enableUIAfterLoaded()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get locations around you at this time.", view: self)
                }
            }else {
                self.enableUIAfterLoaded()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get locations around you at this time.", view: self)
            }
        }
    }
    
    
    // Pagination for local locations
    func getMoreLocalLocations(){
        self.view.isUserInteractionEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.view.isUserInteractionEnabled = true
            return
        }else if self.localLocations.count == 0 {
            // If there are no local locations currently in the view, then we obivously do not want to load any more
            // If we did not have this, postCount declared below would be nil because there is no lastLocalLocation
            self.view.isUserInteractionEnabled = true
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        // We need this variable passed so we know the smallest amount of posts on a location
        let postCount = self.lastLocalLocation!.postCount
        let parameters = [
            "numberOfPosts": postCount,
            "locationIds": self.localLocationsIds,
            "longitude": self.longitude,
            "latitude": self.latitude
            ] as [String : Any]
        
        // POST /locations/top/global/more
        Alamofire.request("http://192.168.0.111:8080/locations/loc/more", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    // All the posts are the first array in the array
                    let count = json.count
                    var numberOfImages = count
                    var tempLocationImages = [UIImage?](repeating: nil, count: count)
                    
                    // If there are no locations we want to exit
                    if count == 0 {
                        self.view.isUserInteractionEnabled = true
                        return
                    }
                    
                    for(index, location): (String, JSON) in json{
                        let location = LocalLocation(_id: location["_id"].stringValue, userId: location["userID"].stringValue, name: location["name"].stringValue, postCount: location["numberOfPosts"].int!, longitude: location["loc"][0].double!, latitude: location["loc"][1].double!, imageExt: location["imageExt"].stringValue)
                        
                        // We want to add the post even before the images come in
                        self.localLocations.append(location)
                        self.localLocationsIds.append(location._id)
                        self.lastLocalLocation = location
                        
                        // We want to use a stock image if there is no extension
                        if location.imageExt == "" {
                            tempLocationImages[Int(index)!] = UIImage(named: "locationPictureComingSoon")
                            
                            numberOfImages -= 1
                            if numberOfImages == 0 {
                                print("Done getting images")
                                self.localLocationsImages.append(contentsOf: tempLocationImages as! [UIImage])
                                self.tableView.reloadData()
                                self.view.isUserInteractionEnabled = true
                            }
                        }else {
                            // We stored the image key as an extension in the Post
                            let S3DownloadKeyName: String = location.imageExt
                            let expression = AWSS3TransferUtilityDownloadExpression()
                            
                            // Here is where we will want to do stuff with the image data when it comes back asynchronously
                            // We are going to need to do something with the error
                            self.completionHandler = { (task, location, data, error) -> Void in
                                DispatchQueue.main.async(execute: {
                                    if ((error) != nil){
                                        self.view.isUserInteractionEnabled = true
                                        AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get locations around you at this time.", view: self)
                                    }else{
                                        // No error so now we have the data
                                        let downloadedImage = UIImage(data: data!)
                                        tempLocationImages[Int(index)!] = downloadedImage!
                                        
                                        numberOfImages -= 1
                                        if numberOfImages == 0{
                                            print("Done getting images")
                                            self.localLocationsImages.append(contentsOf: tempLocationImages as! [UIImage])
                                            self.tableView.reloadData()
                                            self.view.isUserInteractionEnabled = true
                                        }
                                    }
                                })
                            }
                            
                            let transferUtility = AWSS3TransferUtility.default()
                            transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
                                if let _ = task.result {
                                    print("Download Starting!")
                                }else {
                                    self.view.isUserInteractionEnabled = true
                                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get locations around you at this time.", view: self)                            }
                                
                                return nil
                            })
                        }
                    }
                }else {
                    self.view.isUserInteractionEnabled = true
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get locations around you at this time.", view: self)
                }
            }else {
                self.view.isUserInteractionEnabled = true
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get locations around you at this time.", view: self)
            }
        }
    }
    
    
    func getGlobalLocations(){
        self.view.isUserInteractionEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.enableUIAfterLoaded()
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        Alamofire.request("http://192.168.0.111:8080/locations/top/global", method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    // All the posts are the first array in the array
                    let count = json.count
                    var numberOfImages = count
                    
                    // We need temp arrays instead of removing all of the data before I get new data
                    // This prevents index out of range
                    var tempLocations = [GlobalLocation]()
                    var tempLocationIds = [String]()
                    var tempLocationImages = [UIImage?](repeating: nil, count: count)
                    
                    // If there are no locations we want to exit
                    if count == 0 {
                        self.enableUIAfterLoaded()
                        AppDelegate.getAppDelegate().alert(title: "No locations here yet.", message: "Top global locations will appear here soon", view: self)
                        return
                    }
                    
                    for(index, location): (String, JSON) in json{
                        let location = GlobalLocation(_id: location["_id"].stringValue, userId: location["userID"].stringValue, name: location["name"].stringValue, postCount: location["numberOfPosts"].int!, longitude: location["loc"][0].double!, latitude: location["loc"][1].double!, imageExt: location["imageExt"].stringValue)
                        
                        // We want to add the location even before the images come in
                        tempLocations.append(location)
                        tempLocationIds.append(location._id)
                        self.lastGlobalLocation = location

                        // We want to use a stock image if there is no extension
                        if location.imageExt == "" {
                            tempLocationImages[Int(index)!] = UIImage(named: "locationPictureComingSoon")
                            
                            numberOfImages -= 1
                            if numberOfImages == 0 {
                                print("Done getting images")
                                // We got the images so now we want to assign the data to the actual arrays
                                self.globalLocations = tempLocations
                                self.globalLocationIds = tempLocationIds
                                self.globalLocationsImages = tempLocationImages as! [UIImage]

                                self.enableUIAfterLoaded()
                            }
                        }else {
                            // We stored the image key as an extension in the Post
                            let S3DownloadKeyName: String = location.imageExt
                            let expression = AWSS3TransferUtilityDownloadExpression()
                            
                            // Here is where we will want to do stuff wit hthe image data when it comes back asynchronously
                            // We are going to need to do something with the error
                            self.completionHandler = { (task, location, data, error) -> Void in
                                DispatchQueue.main.async(execute: {
                                    if ((error) != nil){
                                        self.enableUIAfterLoaded()
                                        AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get the top global locations at this time", view: self)
                                    }else{
                                        // No error so now we have the data
                                        let downloadedImage = UIImage(data: data!)
                                        tempLocationImages[Int(index)!] = downloadedImage!
                                        
                                        numberOfImages -= 1
                                        if numberOfImages == 0{
                                            print("Done getting images")
                                            // We got the images so now we want to assign the data to the actual arrays
                                            self.globalLocations = tempLocations
                                            self.globalLocationIds = tempLocationIds
                                            self.globalLocationsImages = tempLocationImages as! [UIImage]

                                            self.enableUIAfterLoaded()
                                        }
                                    }
                                })
                            }
                            
                            let transferUtility = AWSS3TransferUtility.default()
                            transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
                                if let _ = task.result {
                                    print("Download Starting!")
                                }else {
                                    self.enableUIAfterLoaded()
                                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get the top global locations at this time", view: self)
                                }
                                
                                return nil
                            })
                        }
                    }
                }else {
                    self.enableUIAfterLoaded()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get the top global locations at this time", view: self)
                }
            }else {
                self.enableUIAfterLoaded()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get the top global locations at this time", view: self)
            }
        }
    }
    
    
    // Pagination for global locations
    func getMoreGlobalLocations(){
        self.view.isUserInteractionEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.view.isUserInteractionEnabled = true
            return
        }else if self.globalLocations.count == 0 {
            // If there are no global locations currently in the view, then we obivously do not want to load any more
            // If we did not have this, postCount declared below would be nil because there is no lastGlobalLocation
            self.view.isUserInteractionEnabled = true
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        // We need this variable passed so we know the smallest amount of posts on a location
        let postCount = self.lastGlobalLocation!.postCount
        let parameters = [
            "numberOfPosts": postCount,
            "locationIds": self.globalLocationIds,
            ] as [String : Any]
        
        // POST /locations/top/global/more
        Alamofire.request("http://192.168.0.111:8080/locations/top/global/more", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    // All the posts are the first array in the array
                    let count = json.count
                    var numberOfImages = count
                    var tempLocationImages = [UIImage?](repeating: nil, count: count)
                    
                    // If there are no locations we want to exit
                    if count == 0 {
                        self.view.isUserInteractionEnabled = true
                        return
                    }
                    
                    for(index, location): (String, JSON) in json{
                        let location = GlobalLocation(_id: location["_id"].stringValue, userId: location["userID"].stringValue, name: location["name"].stringValue, postCount: location["numberOfPosts"].int!, longitude: location["loc"][0].double!, latitude: location["loc"][1].double!, imageExt: location["imageExt"].stringValue)
                        
                        // We want to add the post even before the images come in
                        self.globalLocations.append(location)
                        self.globalLocationIds.append(location._id)
                        self.lastGlobalLocation = location

                        // We want to use a stock image if there is no extension
                        if location.imageExt == "" {
                            tempLocationImages[Int(index)!] = UIImage(named: "locationPictureComingSoon")
                            
                            numberOfImages -= 1
                            if numberOfImages == 0 {
                                print("Done getting images")
                                self.globalLocationsImages.append(contentsOf: tempLocationImages as! [UIImage])
                                self.tableView.reloadData()
                                self.view.isUserInteractionEnabled = true
                            }
                        }else {
                            // We stored the image key as an extension in the Post
                            let S3DownloadKeyName: String = location.imageExt
                            let expression = AWSS3TransferUtilityDownloadExpression()
                            
                            // Here is where we will want to do stuff wit hthe image data when it comes back asynchronously
                            // We are going to need to do something with the error
                            self.completionHandler = { (task, location, data, error) -> Void in
                                DispatchQueue.main.async(execute: {
                                    if ((error) != nil){
                                        self.view.isUserInteractionEnabled = true
                                        AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get the top global locations at this time", view: self)
                                    }else{
                                        // No error so now we have the data
                                        let downloadedImage = UIImage(data: data!)
                                        tempLocationImages[Int(index)!] = downloadedImage!
                                        
                                        numberOfImages -= 1
                                        if numberOfImages == 0{
                                            print("Done getting images")
                                            self.globalLocationsImages.append(contentsOf: tempLocationImages as! [UIImage])
                                            self.tableView.reloadData()
                                            self.view.isUserInteractionEnabled = true
                                        }
                                    }
                                })
                            }
                            
                            let transferUtility = AWSS3TransferUtility.default()
                            transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
                                if let _ = task.result {
                                    print("Download Starting!")
                                }else {
                                    self.view.isUserInteractionEnabled = true
                                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get the top global locations at this time", view: self)
                                }
                                
                                return nil
                            })
                        }
                    }
                }else {
                    self.view.isUserInteractionEnabled = true
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get the top global locations at this time", view: self)
                }
            }else {
                self.view.isUserInteractionEnabled = true
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to get the top global locations at this time", view: self)
            }
        }
    }
    
    
    func flagLocation(row: Int) {
        self.view.isUserInteractionEnabled = false
        self.activityIndicatorView.startAnimating()
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.enableUIAfterLoaded()
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        //We need to check which picker view we are in so we know which id to pass
        var parameters: [String:Any]
        
        if self.segControl.selectedSegmentIndex == 0{
            parameters = [
                "_id": self.localLocations[row]._id
            ]
        }else{
            parameters = [
                "_id": self.globalLocations[row]._id
            ]
        }
        
        Alamofire.request("http://192.168.0.111:8080/location/flag", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                self.enableUIAfterLoaded()
                AppDelegate.getAppDelegate().alert(title: "Thank you.", message: "We will be looking into this location.", view: self)
            }else {
                self.enableUIAfterLoaded()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to flag the location at this time", view: self)
            }
        }
    }
    
    
    // MARK: - View Functionality
    @IBAction func pendingLocations(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "pendingLocationsVC") as! AddLocationViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func map(row: Int) {
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "mapVC") as! PopupMapViewController
        
        if segControl.selectedSegmentIndex == 0 {
            vc.latitude = String(self.localLocations[row].latitude)
            vc.longitude = String(self.localLocations[row].longitude)
            vc.locationName = self.localLocations[row].name
        } else {
            vc.latitude = String(self.globalLocations[row].latitude)
            vc.longitude = String(self.globalLocations[row].longitude)
            vc.locationName = self.globalLocations[row].name
        }
        
        self.present(vc, animated: false, completion: nil)
    }
    
    
    func camera(row: Int) {
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "cameraVC") as! CameraViewController
        
        if segControl.selectedSegmentIndex == 0{
            vc.locationId = self.localLocations[row]._id
            vc.locationName = self.localLocations[row].name
        }
        
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func segmentedControlChanged(_ sender: Any) {
        // Here we want to load local locations everytime the seg control goes to local
        // We dont want to load the global locations everytime so that is why we have these checks
        if segControl.selectedSegmentIndex == 0{
            // We only want to animate on initial load and seg control switch
            self.activityIndicatorView.startAnimating()
            
            // We need to enable start updating the location again because we need the location before updating local locations
            if CLLocationManager.locationServicesEnabled() {
                // Here we need to reset the variables so we only get the location once
                self.hasFoundLocation = false
                locationManager.startUpdatingLocation()
            }

        }else if segControl.selectedSegmentIndex == 1 && self.hasGottenGlobalLocations == false{
            self.activityIndicatorView.startAnimating()
            self.hasGottenGlobalLocations = true
            getGlobalLocations()
        }else if segControl.selectedSegmentIndex == 1 && self.hasGottenGlobalLocations == true{
            // We dont want to requery the API so we just update the data we already have
            self.tableView.reloadData()
        }
        
        // Sets header label text when you change the selected segment
        if segControl.selectedSegmentIndex == 0 {
            headerLabel.text = "Local Locations"
        }else {
            headerLabel.text = "Global Locations"
        }
    }
    
    
    // This handles infinite scroll
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y;
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        
        // Hits the bottom
        if ((currentOffset > 0) && (maximumOffset - currentOffset) <= 10) {
            
            // Check which seg control index
            if segControl.selectedSegmentIndex == 0{
                getMoreLocalLocations()
            }else{
                getMoreGlobalLocations()
            }
        }
    }
    
    
    // MARK: - UI and misc
    func customizeUI() {
        // Sets segmented control customizations like height, color, text
        segControl.tintColor = pinkColor
        segControl.layer.cornerRadius = 5
        segControl.backgroundColor = skyBlueColor
        
        if segControl.selectedSegmentIndex == 0 {
            headerLabel.text = "Local Locations"
        }else {
            headerLabel.text = "Global Locations"
        }
        
        // Sets the section title sperators where they need to be on all screens
        seperator1.frame = CGRect(x: 0, y: 10, width: (self.view.frame.width - 139) / 2 , height: 1)
        seperator2.frame = CGRect(x: ((self.view.frame.width - 139) / 2) + 139 , y: 10, width: (self.view.frame.width - 139) / 2, height: 1)
    }
    
    
    func enableUIAfterLoaded() {
        self.tableView.reloadData()
        self.view.isUserInteractionEnabled = true
        self.activityIndicatorView.stopAnimating()
        self.refreshControl.endRefreshing()
    }
    
    
    // We needed to add this so it doesnt refresh until they let go
    // Without this it would refresh once it hit a certain point
    // Then it would glitch out or jump because the alamofire request was firing right away
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // If we dont check for refreshing, it will call this everytime we stop scroll in the tableview
        if refreshControl.isRefreshing {
            refresh()
        }
    }
    
    
    func refresh() {
        // Decides which locations we want to refresh depending on seg control
        if segControl.selectedSegmentIndex == 0{
            // We need to enable start updating the location again because we need the location before updating local locations
            if CLLocationManager.locationServicesEnabled() {
                // Here we need to reset the variables so we only get the location once
                self.hasFoundLocation = false
                locationManager.startUpdatingLocation()
            }
        }else {
            getGlobalLocations()
        }
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    // MARK: - Location Services
    // This function gets called when we assign the CLLocation delegate
    // This acts a a view did load for getting posts ... sort of
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
       if status == .denied || status == .restricted {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "locationServicesVC") as! LocationServicesViewController
            self.present(vc, animated: true, completion: nil)
        }else if status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if hasFoundLocation == false {
            // Only requests API once to be able to load pending locations
            print("Got Location")
        
            let loc = locations[0]
            let myLocation = CLLocationCoordinate2D(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude)
            
            // Now that we have the locatons lets stop it
            locationManager.stopUpdatingLocation()
            
            // Add location to tab bar controller so it can be used in the map view
            // This is better so we don't have to re-get the users location every time they switch to the map
            let tbvc = self.tabBarController  as! TabBarViewController
            tbvc.longitude = loc.coordinate.longitude
            tbvc.latitude = loc.coordinate.latitude
            
            latitude  = String(myLocation.latitude)
            longitude = String(myLocation.longitude)
            
            // We dont want to get the location anymore so we ping the API for the locations
            hasFoundLocation = true
            getLocalLocations()
        }else {
            //self.activityIndicatorView.stopAnimating()
            locationManager.stopUpdatingLocation()
        }
    }
}
