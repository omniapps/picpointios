//
//  PopupMapViewSegue.swift
//  PicPoint
//
//  Created by Devin Miller on 1/22/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import Foundation
import UIKit

class CustomSegue: UIStoryboardSegue {
    
    override func perform() {
        let src = self.source
        let dst = self.destination
        src.present(dst, animated: false, completion: nil)
    }
    
}
