//
//  PendingLocation.swift
//  PicPoint
//
//  Created by Devin Miller on 1/14/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import Foundation

class PendingLocation {
    
    var _id = ""
    var userId = ""
    var name = ""
    var imageExt = ""
    var pendingLocationImage = UIImage()
    
    init(_id: String, userId: String, name: String, imageExt: String){
        self._id = _id
        self.userId = userId
        self.name = name
        self.imageExt = imageExt
    }
}
