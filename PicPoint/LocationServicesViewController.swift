//
//  LocationServicesViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/13/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit
import CoreLocation

class LocationServicesViewController: UIViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    @IBOutlet var okay: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
    }
    
    
    @IBAction func okayButton(_ sender: Any) {
        if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied {
            let alertController = UIAlertController (title: "Disabled Location Access", message: "Make sure to change location from 'Never' to 'While Using the App'.", preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title: "Go to Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)")
                    })
                }
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alertController.addAction(cancelAction)
            
            present(alertController, animated: true, completion: nil)
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    // Makes status bar black
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}
