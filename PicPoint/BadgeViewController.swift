//
//  BadgeViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/20/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//  Refractored - 1/27/17
//

import UIKit
import Alamofire
import SwiftyJSON
import AWSS3
import AWSCore
import NVActivityIndicatorView

class BadgeViewController: UIViewController, UITextViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet weak var more: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var charactersLeft: UILabel!
    @IBOutlet weak var bio: UITextView!
    @IBOutlet weak var votes: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var badgeThumbnailPicture: UIImageView!
    
    
    let pinkColor = UIColor(colorLiteralRed: 252/255, green: 96/255, blue: 97/255, alpha: 1.0)
    let grayColor = UIColor(colorLiteralRed: 211/255, green: 211/255, blue: 211/255, alpha: 1.0)
    let skyBlueColor = UIColor(colorLiteralRed: 230/255, green: 246/255, blue: 252/255, alpha: 1.0)
    let keychain = KeychainWrapper()
    let str = "Add text to your badge here..."
    var myPosts = [Post]()
    var myPostsImages = [UIImage]()
    var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    let S3BucketName: String = "picpoint"
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    
    // MARK: - View Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicatorView = ActivityView.instantiateView(view: self.view)
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.allowsMultipleSelection = false
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.bio.delegate = self
        
        self.username.text = self.keychain.myObject(forKey: kSecAttrAccount) as? String
        
        // We only want to hide the UI during an initial load while getting the information
        self.hideUIWhileLoading()
        getMyInformation()
        
        customizeUI()
    }
    
    
    // MARK: - Collection View
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myPosts.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! BadgeCollectionViewCell
        
        cell.locName.text = myPosts[indexPath.row].locName
        cell.numberOfVotes.text = "\(myPosts[indexPath.row].votes) votes"
        cell.imageView.image = myPostsImages[indexPath.row]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        deleteMyPost(indexPathRow: indexPath.row)
    }
    
    
    // MARK: - Alamofire Requests
    func getMyInformation() {
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.unhideUIAfterLoaded()
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        Alamofire.request("http://192.168.0.111:8080/user/badge/information", method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            print(response.response?.statusCode as Any)
            
            if response.response?.statusCode == 200 {
                // We need reset all of the data for posts
                self.myPosts.removeAll()
                self.myPostsImages.removeAll()
                
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    // All the posts are the second array in the array
                    let count = json[1].count
                    var numberOfImages = count
                    var tempPostImages = [UIImage?](repeating: nil, count: count)
                    
                    // User information
                    // Sets bio text and character count if a user has never updated their description
                    if json[0]["description"].stringValue == "" {
                        self.bio.text = self.str
                        self.bio.textColor = UIColor.lightGray
                        self.charactersLeft.text = String(200)
                    } else {
                        self.bio.text = json[0]["description"].stringValue
                        self.charactersLeft.text = String(200 - self.bio.text.characters.count)
                    }
                    self.votes.text = json[0]["numberOfVotes"].stringValue + " votes"
                    
                    // After we get the basic info, we want to get their profile picture
                    // We dont want to get the image though if they havent changed the default extension ("")
                    // This means they have not uploaded an image
                    let imageExt = json[0]["profileImageExt"].stringValue
                    if imageExt != "" && count > 0 {
                        // Since there is an image to get
                        // We want to make sure that we switch off the activityIndicator after we get the posts
                        self.getProfilePicture(imageExt: imageExt, postsIncluded: true)
                    }else if imageExt != "" && count == 0 {
                        // Since there is an image to get
                        // We want to make sure that we switch off the activityIndicator after the pro pic is fetched
                        self.getProfilePicture(imageExt: imageExt, postsIncluded: false)
                    }else if imageExt == "" && count == 0 {
                        // Since we dont have a pro pic to get and count is 0
                        // We just want to switch off the activity Indicator here
                        self.collectionView.reloadData()
                        self.unhideUIAfterLoaded()
                        return
                    }
                    
                    // Look to improve this in the future
                    for (index, object):(String, JSON) in (json[1]){
                        
                        // These are posts
                        let myPost = Post(_id: object["_id"].stringValue, votes: object["numberOfVotes"].int!, userId: object["userId"].stringValue, username: object["username"].stringValue, locName: object["locName"].stringValue, locId: object["locId"].stringValue, imageExt: object["imageExt"].stringValue, description: object["description"].stringValue)
                        
                        // We stored the image key as an extension in the Post
                        let S3DownloadKeyName: String = myPost.imageExt
                        let expression = AWSS3TransferUtilityDownloadExpression()
                       
                        // Here is where we will want to do stuff with the image data when it comes back asynchronously
                        self.completionHandler = { (task, location, data, error) -> Void in
                            DispatchQueue.main.async(execute: {
                                if ((error) != nil){
                                    self.collectionView.reloadData()
                                    self.unhideUIAfterLoaded()
                                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We can not get your posts at this time.", view: self)
                                }
                                else{
                                    // No error so now we have the data
                                    let downloadedImage = UIImage(data: data!)
                                    tempPostImages[Int(index)!] = downloadedImage!
                                    
                                    // We want to keep track of how many images are left so we know when to reload
                                    numberOfImages -= 1
                                    if numberOfImages == 0{
                                        print("Done getting images")
                                        self.myPostsImages = tempPostImages as! [UIImage]
                                        self.collectionView.reloadData()
                                        self.unhideUIAfterLoaded()
                                    }
                                }
                            })
                        }
                        
                        let transferUtility = AWSS3TransferUtility.default()
                        transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
                            if let _ = task.result {
                                print("Download Starting!")
                            }else{
                                self.collectionView.reloadData()
                                self.unhideUIAfterLoaded()
                                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We can not get your posts at this time.", view: self)
                            }
                            
                            return nil
                        })
                        
                        // We want to append this even though we dont have the images just yet
                        self.myPosts.append(myPost)
                    }
                }else {
                    self.collectionView.reloadData()
                    self.unhideUIAfterLoaded()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to load your posts.", view: self)
                }
            }else {
                self.collectionView.reloadData()
                self.unhideUIAfterLoaded()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to load your posts.", view: self)
            }
        }
    }
    
    
    func getProfilePicture(imageExt: String, postsIncluded: Bool) {
        // We stored the image key as an extension in the Post
        let S3DownloadKeyName: String = imageExt
        let expression = AWSS3TransferUtilityDownloadExpression()
        
        // Here is where we will want to do stuff with the profile picture when it comes back async
        self.completionHandler = { (task, location, data, error) -> Void in
            DispatchQueue.main.async(execute: {
                if ((error) != nil){
                    // We only want to unhide if our posts are not included
                    // Meaning we have no posts to fetch and this image is the last thing needed for the UI
                    if postsIncluded == false {
                        self.unhideUIAfterLoaded()
                    }
                    
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We can not get your profile picture at this time.", view: self)
                }
                else{
                    // No error so now we have the data and want to set the profile picture
                    print("Done getting profile image")
                    self.badgeThumbnailPicture.image = UIImage(data: data!)
                    
                    // We only want to unhide if our posts are not included
                    // Meaning we have no posts to fetch and this image is the last thing needed for the UI
                    if postsIncluded == false {
                        self.collectionView.reloadData()
                        self.unhideUIAfterLoaded()
                    }
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
            if let _ = task.result {
                print("Download Starting!")
            }else{
                // We only want to unhide if our posts are not included
                // Meaning we have no posts to fetch and this image is the last thing needed for the UI
                if postsIncluded == false {
                    self.unhideUIAfterLoaded()
                }
                
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We can not get your profile picture at this time.", view: self)
            }
            
            return nil;
        })
    }
    
    
    func deleteMyPost(indexPathRow: Int) {
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        // Here we make a custom UI Alert just for deleting
        let alert = UIAlertController(title: "Are you sure you want to delete this post?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        // Confirms user wants to delete their post
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: {(action: UIAlertAction!) in
            self.view.isUserInteractionEnabled = false
            self.activityIndicatorView.startAnimating()
            
            let headers = [
                "Auth": self.keychain.myObject(forKey: kSecValueData) as! String
            ]
            
            let parameters = [
                "_id": self.myPosts[indexPathRow]._id
            ]
            
            // DELETE /post
            Alamofire.request("http://192.168.0.111:8080/post/delete", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.response?.statusCode == 204 {
                    // Reloading and UI changes will be handeled when the posts are gotten
                    self.getMyInformation()
                }else {
                    self.view.isUserInteractionEnabled = true
                    self.activityIndicatorView.stopAnimating()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to delete your post at this time.", view: self)
                }
            }
        }))
        
        // User does NOT want to delete their post
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: {(action: UIAlertAction!) in
            return
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
  
    func updateDescription(){
        self.view.isUserInteractionEnabled = false
        self.activityIndicatorView.startAnimating()
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.view.isUserInteractionEnabled = true
            self.activityIndicatorView.stopAnimating()
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        let parameters = [
            "description": self.bio.text
        ]
        
        // PUT /user/update/description
        Alamofire.request("http://192.168.0.111:8080/user/update/description", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                self.view.isUserInteractionEnabled = true
                self.activityIndicatorView.stopAnimating()
            }else {
                self.view.isUserInteractionEnabled = true
                self.activityIndicatorView.stopAnimating()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to update your description at this time.", view: self)
            }
        }

    }
    
    
    func logoutUser() {
        self.view.isUserInteractionEnabled = false
        self.activityIndicatorView.startAnimating()
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.view.isUserInteractionEnabled = true
            self.activityIndicatorView.stopAnimating()
            return
        }
        
        let headers = [
            "Auth": self.keychain.myObject(forKey: kSecValueData) as! String
        ]
        
        // DELETE /users/logout
        Alamofire.request("http://192.168.0.111:8080/user/logout", method: .delete, parameters: nil, headers: headers).responseJSON { response in
            print(response.response?.statusCode as Any)
            
            if response.response?.statusCode == 200 || response.response?.statusCode == 204 {
                // We found and deleted the token (200), so we reset the keychain and segue
                // We didnt find a token (204), but we still want to reset and segue
                // This is because it could be a glitch in the server where the database deleted the token 
                // So we just want to reset the keychain anyway because the token is faulty 
                // Should change in the future
                self.resetKeychain()
                self.performSegue(withIdentifier: "logout", sender: self)
            }else {
                self.view.isUserInteractionEnabled = true
                self.activityIndicatorView.stopAnimating()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to log you out at this time.", view: self)
            }
        }
    }
    
    
    // MARK: - More Button
    @IBAction func moreButton(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takePictureAction = UIAlertAction(title: "Edit Profile Pic", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.takeNewProfilePicture()
        })
        let saveAction = UIAlertAction(title: "Save Bio", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.updateDescription()
        })
        let logoutAction = UIAlertAction(title: "Logout", style: .destructive, handler: {(alert: UIAlertAction!) -> Void in
            self.logoutUser()
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alert: UIAlertAction!) -> Void in })
        
        alert.addAction(takePictureAction)
        alert.addAction(saveAction)
        alert.addAction(logoutAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func takeNewProfilePicture() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "thumbnailPicVC") as! ThumbnailPictureViewController
        vc.whichViewTookImage = "badge"
        self.present(vc, animated: true, completion: nil)
    }
    
    
    // MARK: - Text View
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        charactersLeft.text = String(200 - newLength)
        return newLength <= 200
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        charactersLeft.isHidden = false
        if bio.textColor == UIColor.lightGray {
            bio.text = nil
            bio.textColor = UIColor.black
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        charactersLeft.isHidden = true
        if bio.text.isEmpty {
            bio.textColor = UIColor.lightGray
            bio.text = str
        }
    }

    
    // MARK: - UI
    func customizeUI() {
        
        // Setting bio
        bio.layer.borderWidth = 1.5
        bio.layer.borderColor = grayColor.cgColor
        bio.layer.cornerRadius = 5
        charactersLeft.textColor = pinkColor
        charactersLeft.isHidden = true
        
        badgeThumbnailPicture.layer.cornerRadius = badgeThumbnailPicture.frame.width / 2
        badgeThumbnailPicture.layer.masksToBounds = true
    }
    
    
    func hideUIWhileLoading(){
        self.activityIndicatorView.startAnimating()
        
        self.more.isHidden = true
        self.collectionView.isHidden = true
        self.charactersLeft.isHidden = true
        self.bio.isHidden = true
        self.votes.isHidden = true
        self.username.isHidden = true
        self.badgeThumbnailPicture.isHidden = true
    }
    
    
    func unhideUIAfterLoaded(){
        self.view.isUserInteractionEnabled = true
        self.activityIndicatorView.stopAnimating()
        
        self.more.isHidden = false
        self.collectionView.isHidden = false
        self.bio.isHidden = false
        self.votes.isHidden = false
        self.username.isHidden = false
        self.badgeThumbnailPicture.isHidden = false
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    
    // MARK: - Events
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    // MARK: - Keychain
    func deleteAllKeysForSecClass(secClass: CFTypeRef) {
        let dict: [NSString : AnyObject] = [kSecClass : secClass]
        let result = SecItemDelete(dict as CFDictionary)
        assert(result == noErr || result == errSecItemNotFound, "Error deleting keychain data (\(result))")
    }
    
    
    func resetKeychain() {
        self.deleteAllKeysForSecClass(secClass: kSecClassGenericPassword)
        self.deleteAllKeysForSecClass(secClass: kSecClassInternetPassword)
        self.deleteAllKeysForSecClass(secClass: kSecClassCertificate)
        self.deleteAllKeysForSecClass(secClass: kSecClassKey)
        self.deleteAllKeysForSecClass(secClass: kSecClassIdentity)
    }
}
