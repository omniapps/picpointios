//
//  PostsViewController.swift
//  PicPoint
//
//  Created by Devin Miller on 1/11/17.
//  Copyright © 2017 Omni Apps. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON
import Alamofire
import AWSS3
import AWSCognito
import AWSCore
import NVActivityIndicatorView

class PostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate{
    
    
    @IBOutlet weak var cameraBTN: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var header: UIView!
    
    
    let keychain = KeychainWrapper()
    let refreshControl = UIRefreshControl()
    let S3BucketName: String = "picpoint"
    var posts = [Post]()
    var votes = [Vote]()
    var postIds = [String]()
    var lastPost: Post?
    var isActive = Bool()
    var postImages = [UIImage]()
    var longitude = String()
    var latitude  = String()
    var locationId = String()
    var locationName = String()
    var postCount = Int()
    var hasFoundLocation = false
    var hideCamera = false
    var completionHandler: AWSS3TransferUtilityDownloadCompletionHandlerBlock?
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    
    // MARK: - View Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicatorView = ActivityView.instantiateView(view: self.view)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.toggleOverlay))
        self.view.addGestureRecognizer(tap)
        
        // Might have to update to .refreshControl = self.refreshControl
        self.tableView.addSubview(self.refreshControl)
        
        customizeUI()
        
        // We only want to show the activity view on the initial load
        self.activityIndicatorView.startAnimating()
        getPosts()
    }
    
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "post") as! PostTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        // We might want to set a stock image here and just get each image asynchronously
        if self.postImages.count == self.posts.count {
            cell.postImage.image = self.postImages[indexPath.row]
        }
        
        cell.usernameBTN.setTitle(posts[indexPath.row].username, for: .normal)
        cell.votes.text = String(posts[indexPath.row].votes)
        cell.descriptionText.text = posts[indexPath.row].description
       
        // Check if the user voted on each post and if so update the UI accordingly
        if self.posts[indexPath.row].currentUserHasVoted == true {
            cell.currentUserHasVoted = true
            if self.posts[indexPath.row].voteValue == 1 {
                cell.upVote.isEnabled = false
                cell.downVote.isEnabled = true
                cell.upVote.setImage(UIImage(named: "upVoteHighlighted.png"), for: .normal)
                cell.downVote.setImage(UIImage(named: "downVote.png"), for: .normal)
            }else if self.posts[indexPath.row].voteValue == -1 {
                cell.downVote.isEnabled = false
                cell.upVote.isEnabled = true
                cell.downVote.setImage(UIImage(named: "downVoteHighlighted.png"), for: .normal)
                cell.upVote.setImage(UIImage(named: "upVote.png"), for: .normal)
            }
        }else {
            cell.currentUserHasVoted = false
        }
        
        // Passes what cell row was interacted with when someone Up or Down votes, flags or checks the users profile
        // Look to improve this
        cell.up = { (cell) in
            self.vote(row: indexPath.row, vote: 1)
        }
        cell.down = { (cell) in
            self.vote(row: indexPath.row, vote: -1)
        }
        cell.flag = { (cell) in
            self.flagPost(row: indexPath.row)
        }
        cell.username = { (cell) in
            self.goToBadge(row: indexPath.row)
        }
        
        return cell
    }
    
    
    // MARK: - Alamofire Requests
    func getPosts() {
        self.view.isUserInteractionEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.unhideUIAfterLoaded()
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        // GET /posts/loc?
        Alamofire.request("http://192.168.0.111:8080/posts/loc?locId=\(locationId)", method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    // All the posts are the first array in the array
                    let count = json[0].count
                    var amountOfImages = count
                    
                    // We want to use temp arrays until we get the data forsure
                    var tempPosts = [Post]()
                    var tempPostIds = [String]()
                    var tempVotes = [Vote]()
                    var tempPostImages = [UIImage?](repeating: nil, count: count)
                    
                    // If there are no posts then we want to exit this function
                    if count == 0 {
                        self.unhideUIAfterLoaded()
                        AppDelegate.getAppDelegate().alert(title: "No posts here yet.", message: "If you are in this area, be the first to post to get this location going!", view: self)
                        return
                    }
                    
                    // Look to improve this in the future
                    // Basically the posts are in the first index and the votes are in the second
                    // That is why we need 2 for loops
                    for x in 0...1{
                        for (index, object):(String, JSON) in (json[x]){
                            if x == 0 {
                                let post = Post(_id: object["_id"].stringValue, votes: object["numberOfVotes"].int!, userId: object["userId"].stringValue, username: object["username"].stringValue, locName: object["locName"].stringValue, locId: object["locId"].stringValue, imageExt: object["imageExt"].stringValue, description: object["description"].stringValue)
                                
                                // Here we need to add all of the postIds so we know which posts we already have
                                // Also we want to keep track of the last post which happens to be the post with the least amount of votes
                                tempPostIds.append(object["_id"].stringValue)
                                self.lastPost = post
                                tempPosts.append(post)
                                
                                //We stored the image key as an extension in the Post
                                let S3DownloadKeyName: String = post.imageExt
                                let expression = AWSS3TransferUtilityDownloadExpression()
                                
                                //Here is where we will want to do stuff wit hthe image data when it comes back asynchronously
                                //We are going to need to do something with the error
                                self.completionHandler = { (task, location, data, error) -> Void in
                                    DispatchQueue.main.async(execute: {
                                        if ((error) != nil){
                                            self.unhideUIAfterLoaded()
                                            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We could not get the posts at this time.", view: self)
                                        }else{
                                            //No error so now we have the data
                                            let downloadedImage = UIImage(data: data!)
                                            tempPostImages[Int(index)!] = downloadedImage!
                                            
                                            amountOfImages -= 1
                                            if amountOfImages == 0{
                                                print("Done getting images")
                                                // Assign the temp array data to the actual arrays
                                                self.posts = tempPosts
                                                self.postIds = tempPostIds
                                                self.votes = tempVotes
                                                self.postImages = tempPostImages as! [UIImage]
                                                
                                                // After we get new posts we want to check if the user has voted on them
                                                self.checkWhichPostsUserVotedOn()
                                                
                                                self.unhideUIAfterLoaded()
                                            }
                                        }
                                    })
                                }
                                
                                let transferUtility = AWSS3TransferUtility.default()
                                transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
                                    if let _ = task.result {
                                        print("Download Starting!")
                                    }else {
                                        self.unhideUIAfterLoaded()
                                        AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We could not get the posts at this time.", view: self)
                                    }
                                    
                                    return nil
                                })
                            }else{
                                // At some point we may want to make sure that we have a check in place to assure that we dont crash the app here
                                // This would be because for some mysterious reason all of the images come back and are fetched before
                                // The votes get added to the array
                                let vote = Vote(voteValue: object["voteValue"].int!, userId: object["userId"].stringValue, postId: object["postId"].stringValue, locId: object["locId"].stringValue, currentUserHasVoted: false)
                                tempVotes.append(vote)
                            }
                        }
                    }
                }else {
                    self.unhideUIAfterLoaded()
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We could not get the posts at this time.", view: self)
                }
            }else {
                self.unhideUIAfterLoaded()
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We could not get the posts at this time.", view: self)
            }
        }
    }
    
    
    // Pagination for local locations
    func getMorePosts(){
        // Possibly look to change this
        // Not sure if we even need to care about user interaction here
        self.view.isUserInteractionEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.view.isUserInteractionEnabled = true
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        // We need this variable passed so we know the smallest amount of posts on a location
        let numberOfVotes = self.lastPost!.votes
        
        let parameters = [
            "numberOfVotes": numberOfVotes,
            "postIds": self.postIds,
            "locId": self.locationId
            ] as [String : Any]
        
        // POST /posts/loc/more
        Alamofire.request("http://192.168.0.111:8080/posts/loc/more", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    //All the posts are the first array in the array
                    let count = json[0].count
                    var amountOfImages = count
                    
                    // Need to declare temp arrays and then use those to replace actual arrays
                    // This is so we dont get rid of the data before we are sure we got it all
                    var tempVotes = [Vote]()
                    var tempPostImages = [UIImage?](repeating: nil, count: count)
                    
                    // If there are no posts to load then we want to exit and renable interaction
                    if count == 0 {
                        self.view.isUserInteractionEnabled = true
                        return
                    }
                    
                    //Look to improve this in the future
                    for x in 0...1{
                        for (index, object):(String, JSON) in (json[x]){
                            if x == 0 {
                                let post = Post(_id: object["_id"].stringValue, votes: object["numberOfVotes"].int!, userId: object["userId"].stringValue, username: object["username"].stringValue, locName: object["locName"].stringValue, locId: object["locId"].stringValue, imageExt: object["imageExt"].stringValue, description: object["description"].stringValue)
                                
                                // Here we need to add all of the postIds so we know which posts we already have
                                // Also we want to keep track of the last post which happens to be the post with the least amount of votes
                                self.postIds.append(object["_id"].stringValue)
                                self.lastPost = post
                                self.posts.append(post)
                        
                                //We stored the image key as an extension in the Post
                                let S3DownloadKeyName: String = post.imageExt
                                let expression = AWSS3TransferUtilityDownloadExpression()
                                
                                //Here is where we will want to do stuff wit hthe image data when it comes back asynchronously
                                //We are going to need to do something with the error
                                self.completionHandler = { (task, location, data, error) -> Void in
                                    DispatchQueue.main.async(execute: {
                                        if ((error) != nil){
                                            self.view.isUserInteractionEnabled = true
                                            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We could not get the posts at this time.", view: self)
                                        }else{
                                            //No error so now we have the data
                                            let downloadedImage = UIImage(data: data!)
                                            tempPostImages[Int(index)!] = downloadedImage!
                                            
                                            amountOfImages -= 1
                                            if amountOfImages == 0{
                                                print("Done getting images")
                                                self.votes = tempVotes
                                                self.postImages.append(contentsOf: tempPostImages as! [UIImage])
                                                
                                                // After we get new posts we want to check if the user has voted on them
                                                self.checkWhichPostsUserVotedOn()

                                                self.tableView.reloadData()
                                                self.view.isUserInteractionEnabled = true
                                            }
                                        }
                                    })
                                }
                                
                                let transferUtility = AWSS3TransferUtility.default()
                                transferUtility.downloadData(fromBucket: self.S3BucketName, key: S3DownloadKeyName, expression: expression, completionHander: self.completionHandler).continue({ (task) -> AnyObject! in
                                    if let _ = task.result {
                                        print("Download Starting!")
                                    }else {
                                        self.view.isUserInteractionEnabled = true
                                        AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We could not get the posts at this time.", view: self)
                                    }
                                    
                                    return nil
                                })
                                
                            }else{
                                // At some point we may want to make sure that we have a check in place to assure that we dont crash the app here
                                // This would be because for some mysterious reason all of the images come back and are fetched before
                                // The votes get added to the array
                                let vote = Vote(voteValue: object["voteValue"].int!, userId: object["userId"].stringValue, postId: object["postId"].stringValue, locId: object["locId"].stringValue, currentUserHasVoted: false)
                                tempVotes.append(vote)
                            }
                        }
                    }
                }else {
                    self.view.isUserInteractionEnabled = true
                    AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We could not get the posts at this time.", view: self)
                }
            }else {
                self.view.isUserInteractionEnabled = true
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We could not get the posts at this time.", view: self)
            }
        }
        
    }
    
    
    func vote(row: Int, vote: Int) {
        self.view.isUserInteractionEnabled = false
        
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            self.view.isUserInteractionEnabled = true
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        // Assign JSON parameters
        let parameters = ["postId"    : posts[row]._id,
                          "locId"     : locationId,
                          "voteValue" : vote           ] as [String : Any]
        
        // POST /post/vote
        Alamofire.request("http://192.168.0.111:8080/post/vote", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                // After voting I want to update all of the values for the post I voted on
                // This depends on whether or not they have voted before
                if self.posts[row].currentUserHasVoted == true {
                    self.posts[row].currentUserHasVoted = true
                    self.posts[row].voteValue = vote
                    self.posts[row].votes += (vote * 2)
                }else if self.posts[row].currentUserHasVoted == false {
                    self.posts[row].currentUserHasVoted = true
                    self.posts[row].voteValue = vote
                    self.posts[row].votes += vote
                }
                
                self.view.isUserInteractionEnabled = true
            }else {
                self.view.isUserInteractionEnabled = true
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "Sorry, we were not able to submit your vote at this time.", view: self)
            }
        }
    }
    
    
    func checkWhichPostsUserVotedOn() {
        // Go through all of the posts and see if there is a vote that belongs to it
        for post in self.posts {
            for vote in votes {
                // If there is a vote where the id's match
                // Do something with the values
                if vote.postId == post._id {
                    // There was a match so we need to update the posts properties
                    if vote.voteValue == 1 {
                        post.currentUserHasVoted = true
                        post.voteValue = 1
                        break
                    }else if vote.voteValue == -1 {
                        post.currentUserHasVoted = true
                        post.voteValue = -1
                        break
                    }
                }
            }
        }
    }
    
    
    func flagPost(row: Int){
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        let headers = [ "Auth": keychain.myObject(forKey: kSecValueData) as! String ]
        
        let parameters = [
            "_id": self.posts[row]._id
        ]
        
        Alamofire.request("http://192.168.0.111:8080/post/flag", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.response?.statusCode == 200 {
                AppDelegate.getAppDelegate().alert(title: "Thank you.", message: "We will be looking into this post.", view: self)
            }else {
                AppDelegate.getAppDelegate().alert(title: "Oops.", message: "We were unable to flag the post at this time", view: self)
            }
        }
    }

    
    // MARK: - Feed Functionality
    // This handles infinite scroll
    // Could improve this
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y;
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        
        // Hits the bottom
        if ((currentOffset > 0) && (maximumOffset - currentOffset) <= 10) {
            getMorePosts()
        }
    }
    
    
    func toggleOverlay(recognizer: UITapGestureRecognizer) {
        if isActive == false {
            isActive = true
            UIView.animate(withDuration: 1.0, animations: {
                self.header.alpha = 0.6
            })
        } else {
            isActive = false
            UIView.animate(withDuration: 1.0, animations: {
                self.header.alpha = 0.1
            })
        }
    }
    
    
    func goToBadge(row: Int){
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "badgeVC") as! PublicBadgeViewController
        vc.usernamePassedFromPosts = self.posts[row].username
        vc.userIdPassedFromPosts = self.posts[row].userId
        self.present(vc, animated: true, completion: nil)
    }
    
    
    // Segues to camera so user can post a picture in that location
    @IBAction func cameraButton(_ sender: Any) {
        // Checks if there is no internet, if there is none, it breaks out of function
        if AppDelegate.getAppDelegate().reach.connectedToNetwork() == false {
            AppDelegate.getAppDelegate().alert(title: "Oops.", message: "That's embarrassing... you seem to have lost your internet. Please go find some in order to continue using the app.", view: self)
            return
        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "cameraVC") as! CameraViewController
        vc.locationId = locationId
        vc.locationName = locationName
        self.present(vc, animated: true, completion: nil)
    }
    

    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - UI
    func customizeUI() {
        location.text = locationName
        isActive = false
        
        if hideCamera{
            cameraBTN.isHidden = true
        }else {
            cameraBTN.isHidden = false
        }
        
        
    }
    
    
    func unhideUIAfterLoaded(){
        self.refreshControl.endRefreshing()
        self.activityIndicatorView.stopAnimating()
        self.view.isUserInteractionEnabled = true
        self.tableView.reloadData()
        self.tableView.isPagingEnabled = true
    }

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    // We need to disable pagination if they are pulling to refresh
    // Pagination caused issues with overlapping refresh control
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if refreshControl.isRefreshing {
            self.tableView.isPagingEnabled = false
        }
    }
    
    // We needed to add this so it doesnt refresh until they let go
    // Without this it would refresh once it hit a certain point
    // Then it would glitch out or jump because the alamofire request was firing right away
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // If we dont check for refreshing, it will call this everytime we stop scroll in the tableview
        if refreshControl.isRefreshing {
            refresh()
        }
    }
    
    
    func refresh() {
        getPosts()
    }
}

